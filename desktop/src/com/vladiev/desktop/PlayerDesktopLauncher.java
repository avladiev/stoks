package com.vladiev.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.vladiev.MyGdxGame;
import com.vladiev.player.PlayerApplication;

/**
 * Created by vladia on 5/13/2016.
 */
public class PlayerDesktopLauncher {
    public static void main(String[] args) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 800;
        config.height = 480;
        new LwjglApplication(new PlayerApplication(), config);
    }
}
