package com.vladiev.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.vladiev.GomoboricApplication;
import com.vladiev.StoksApplication;

/**
 * Created by vladia on 5/13/2016.
 */
public class DesktopGomoboricLauncher {


    public static void main(String[] args) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 800;
        config.height = 480;
        new LwjglApplication(new GomoboricApplication(), config);
    }

}
