package com.vladiev.shape;

import com.sun.org.apache.xerces.internal.impl.dv.xs.DoubleDV;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import com.vladiev.model.Border;
import com.vladiev.util.GeometryUtil;
import com.vladiev.util.Tool;
import org.la4j.Vector;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by mazzachuses on 08.04.16.
 */
public class Shape {

    public enum Type {
        EXTERNAL, INTERNAL
    }

    public static final double DEFAULT_EPSILON = 0.05;

    private List<Vector> deckardBorder;
    private List<Vector> polarBorder;
    private List<Vector> normals;
    private final double epsilon;

    private Double aria;
    private Double perimeter;
    private Vector centerOfMass;

    //for LibGdx
    float[] startPoints;

    private Type type;

    public Shape(List<Vector> deckardBorder, boolean some) {
        this.deckardBorder = deckardBorder;
        this.type = Type.EXTERNAL;
        epsilon = Double.NaN;
    }

    public Shape(List<Vector> deckardBorder, double epsilon) {
        this.deckardBorder = deckardBorder;
        this.epsilon = epsilon;
        this.type = Type.EXTERNAL;
        alignDeckardBorder();
    }

    public Shape(List<Vector> deckardBorder) {
        this(deckardBorder, DEFAULT_EPSILON);
    }


    public void alignDeckardBorder() {
        List<Vector> newBorder = new ArrayList<>();
        newBorder.add(deckardBorder.get(0));
        double perimeter = getPerimeter();
        double epsilon = perimeter* 0.003979;

        if (type == Type.INTERNAL) {
            // epsilon = epsilon / 5;
        } else {
            System.out.println("perimeter=" + perimeter);
            System.out.println(String.format("eps=%f", epsilon));
            System.out.println(String.format("eps/per=%f", epsilon / perimeter));
        }
        for (int i = 1; i < deckardBorder.size() + 1; ) {
            Vector previous = newBorder.get(newBorder.size() - 1);
            int index = i % deckardBorder.size();
            Vector point = deckardBorder.get(index);
            double l = previous.subtract(point).norm();
            if (2 * epsilon < l) {
                Vector direction = point.subtract(previous);
                direction = direction.multiply(epsilon / direction.norm());
                newBorder.add(previous.add(direction));
            } else if (epsilon < l) {
                if (i != deckardBorder.size()) {
                    newBorder.add(point);
                }
                i++;
            } else {
                if (i == deckardBorder.size()) {
                    newBorder = newBorder.subList(1, newBorder.size());
                }
                i++;
            }
        }

        deckardBorder = newBorder;
        resetVar();
        shiftBorder();

    }

    private void shiftBorder() {
        List<Vector> polarBorder = getPolarBorder();
        Double minAngel = Double.POSITIVE_INFINITY;
        int minIndex = 0;
        for (int i = 0; i < polarBorder.size(); ++i) {
            double angel = polarBorder.get(i).get(0);
            if (minAngel > angel) {
                minAngel = angel;
                minIndex = i;
            }
        }
        List<Vector> newDeckardBorder = new ArrayList<>(deckardBorder.subList(minIndex, deckardBorder.size()));
        newDeckardBorder.addAll(deckardBorder.subList(0, minIndex));
        deckardBorder = newDeckardBorder;
        resetVar();
    }

    public void recountBorder(List<Vector> velocities, double time) {
        List<Vector> deckardBorder = getDeckardBorder();
        for (int i = 0; i < deckardBorder.size(); ++i) {
            Vector velocity = velocities.get(i);
            Vector motion = velocity.multiply(time);
            Vector p = deckardBorder.get(i);
            p = p.add(motion);
            deckardBorder.set(i, p);
        }

        resetVar();
        alignDeckardBorder();
    }

    private void resetVar() {
        polarBorder = null;
        normals = null;
        centerOfMass = null;
        aria = null;
        perimeter = null;
    }

    //last point is equals to the first point
    private List<Vector> makePolarBorder() {
        List<Vector> deckardBorder = getDeckardBorder();
        List<Vector> polarBorder = new ArrayList<>(deckardBorder.size());
        for (int i = 0; i < deckardBorder.size(); i++) {
            Vector p = deckardBorder.get(i);
            polarBorder.add(GeometryUtil.toPolar(p));
        }
        //  todo make circle shift
        return polarBorder;
    }


    public List<Vector> getDeckardBorder() {
        return deckardBorder;
    }

    public List<Vector> getPolarBorder() {
        if (polarBorder == null) {
            polarBorder = makePolarBorder();
        }
        return polarBorder;
    }

    public List<Vector> getNormals() {
        if (normals == null) {
            normals = countNormals();
        }
        return normals;
    }

    private List<Vector> countNormals() {
        List<Vector> deckardBorder = getDeckardBorder();
        List<Vector> normals = new ArrayList<>(deckardBorder.size());

        for (int i = 0; i < deckardBorder.size(); ++i) {
            Vector previous = Tool.getPrevious(deckardBorder, i);
            Vector next = Tool.getNext(deckardBorder, i);
            Vector center = deckardBorder.get(i);

            Vector normal = GeometryUtil.countNormal(center, previous, next);
            normal = GeometryUtil.countExternalNormal(normal, previous, center);
            if (type == Type.INTERNAL) {
                normal = normal.multiply(-1.);
            }
            normals.add(normal);
        }
        return normals;
    }

    public double getAria() {
        if (aria == null) {
            aria = GeometryUtil.countArea(getDeckardBorder());
        }
        return aria;
    }


    public double getPerimeter() {
        if (perimeter == null) {
            perimeter = GeometryUtil.countPerimeter(getDeckardBorder());
        }
        return perimeter;
    }

    public Vector getCenterOfMass() {
        if (centerOfMass == null) {
            centerOfMass = GeometryUtil.centerOfMass(getDeckardBorder());
        }
        return centerOfMass;
    }

    //bad practice

    /**
     * @return array where every even element represents the horizontal part of a point, and the following element
     * representing the vertical part
     */
    public float[] getCurrentPoints(Vector offset, double scale) {
        List<Vector> deckardBorder = getDeckardBorder();

        int len = 2 * deckardBorder.size();
        float[] res = new float[len];
        for (int i = 0, j = 0; i < len; i += 2, ++j) {
            Vector p = deckardBorder.get(j).multiply(scale).add(offset);

            res[i] = (float) (p.get(0));
            res[i + 1] = (float) (p.get(1));
        }
        if (startPoints == null) {
            startPoints = res;
        }
        return res;
    }

    public float[] getStartPoints() {
        return startPoints;
    }

    public void markAsInternal() {
        setType(Type.INTERNAL);
    }

    public void setType(Type type) {
        this.type = type;
    }

    public double getEpsilon() {
        return epsilon;
    }
}
