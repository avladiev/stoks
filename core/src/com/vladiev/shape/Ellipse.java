package com.vladiev.shape;

import com.badlogic.gdx.utils.Array;
import com.vladiev.model.Border;
import com.vladiev.util.StoksUtil;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mazzachuses on 31.01.16.
 */
@Deprecated
public class Ellipse {
    double a;  //большая полуось;
    double b; // малая полуось;
    Border deckardBorder;
    Border polarBorder;
    double stepMesh;
    double x0;
    double y0;
    double r0;
    double alpha0;

    //http://dxdy.ru/topic49169-15.html
    double tiltAngle;


    public Ellipse(double a, double b,
                   double x0, double y0, double tiltAngle, double stepMesh) {
        this.a = a;
        this.b = b;
        this.x0 = x0;
        this.y0 = y0;
        this.tiltAngle = tiltAngle;
        this.stepMesh = stepMesh;
        this.r0 = countRadius(x0, y0);
        this.alpha0 = countAngel(x0, y0);
    }


    public Border getDeckardBorder() {
        if (deckardBorder == null) {
            deckardBorder = makeDeckardBorder();
        }
        return deckardBorder;
    }


    public Border getPolarBorder() {
        if (polarBorder == null) {
            polarBorder = makePolarBorder();
        }
        return polarBorder;
    }

    // first quarter only!!!
    private Border makePolarBorder() {
        Border deckardBorder = getDeckardBorder();
        double[] x = deckardBorder.getFirstCoordinates();
        double[] y = deckardBorder.getSecondCoordinates();

        double[] r = new double[x.length];
        double[] angle = new double[x.length];
        for (int i = 0; i < x.length; ++i) {
            r[i] = countRadius(x[i], y[i]);
            angle[i] = countAngel(x[i], y[i]);
        }
        return new Border(angle, r);
    }

    private double countAngel(double x, double y) {
        return Math.atan2(y, x);
    }

    private double countRadius(double x, double y) {
        return Math.sqrt(x * x + y * y);
    }

    //http://compgraphics.info/2D/matrix_rotate.php
    private Border makeDeckardBorder() {
        double[] mesh = StoksUtil.createUniformMesh(0, 2 * Math.PI, stepMesh);
        double[] x = new double[mesh.length];
        double[] y = new double[mesh.length];

        for (int i = 0; i < mesh.length; ++i) {
            double t = mesh[i];
            double tempX = a * Math.cos(t);
            double tempY = b * Math.sin(t);

            x[i] = tempX * Math.cos(tiltAngle) - tempY * Math.sin(tiltAngle) + x0;
            y[i] = tempX * Math.sin(tiltAngle) + tempY * Math.cos(tiltAngle) + y0;
            //x[i] = tempX * Math.cos(tiltAngle) + tempY * Math.sin(tiltAngle) + x0;
            //y[i] = -tempX * Math.sin(tiltAngle) + tempY * Math.cos(tiltAngle) + y0;
        }
        return new Border(x, y);
    }

    //move border
    public void recountBorder(List<Vector> velocities, double time) {
        Border deckardBorder = getDeckardBorder();
        StringBuilder sb = new StringBuilder();
        NumberFormat formatter = new DecimalFormat("#0.00");
        for (int i = 0; i < deckardBorder.length(); ++i) {
            Vector velocity = velocities.get(i);
            Vector motion = velocity.multiply(time);
            double[] x = deckardBorder.getFirstCoordinates();
            double[] y = deckardBorder.getSecondCoordinates();
            x[i] += motion.get(0);
            y[i] += motion.get(1);
            sb.append("(").append(formatter.format(x[i])).append(",").append(formatter.format(y[i])).append(")");
        }

        System.out.println("New Border: " + sb);
        System.out.println("Velocities:" + velocities);
    }

    public  void recount(){
        List<Double> first = new ArrayList<>();
        List<Double> second = new ArrayList<>();
        first.add(deckardBorder.getFirstCoordinates()[0]);
        second.add(deckardBorder.getSecondCoordinates()[0]);
        double epsion = 0.5;
        for (int i = 1; i < deckardBorder.length(); ) {
            Vector left = getPoint(first, second);
            Vector point = getPoint(deckardBorder, i);

            double l = left.subtract(point).norm();
            if (l > 2 * epsion) {
                first.add(0.5 * (left.get(0) + point.get(0)));
                second.add(0.5 * (left.get(1) + point.get(1)));
            } else if (l > epsion) {
                first.add(point.get(0));
                second.add(point.get(1));
                i++;
            }else {
                i++;
            }
        }

        double[] firstDoubles = new double[first.size()];
        double[] secondDoubles = new double[first.size()];

        for (int i = 0; i < firstDoubles.length; ++i) {
            firstDoubles[i] = first.get(i);
            secondDoubles[i] = second.get(i);
        }

        Border border = new Border(firstDoubles, secondDoubles);

        this.deckardBorder = border;

        polarBorder = null;
    }



    Vector getPoint(List<Double> first, List<Double> second) {
        int i = second.size()-1;
        return new BasicVector(new double[]{first.get(i), second.get(i)});
    }


    //left relative to the array
    private Vector getLeft(Border border, int index) {
        int resIndex = -1;
        if (index > 0) {
            resIndex = index - 1;
        } else {
            resIndex = border.length() - 1;
        }
        return getPoint(border, resIndex);
    }

    private Vector getPoint(Border border, int resIndex) {
        double x = border.getFirstCoordinates()[resIndex];
        double y = border.getSecondCoordinates()[resIndex];
        return new BasicVector(new double[]{x, y});
    }

    //right relative to the array
    private Vector getRight(Border border, int index) {
        int resIndex = -1;
        if (index < border.length() - 1) {
            resIndex = index + 1;
        } else {
            resIndex = 0;
        }
        return getPoint(border, resIndex);
    }

    public double getR0() {
        return r0;
    }

    public double getAlpha0() {
        return alpha0;
    }

    public double getX0() {
        return x0;
    }

    public double getY0() {
        return y0;
    }

    public double getStepMesh() {
        return stepMesh;
    }
}
