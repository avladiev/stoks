package com.vladiev.shape;

import com.vladiev.model.Border;
import com.vladiev.model.DoubleArray;
import com.vladiev.util.StoksUtil;

import java.util.Arrays;

/**
 * Created by mazzachuses on 29.01.16.
 */
public class Circle {

    private static final double DEFAULT_DELTA = 0.01;

    //параметры  окружности
    //координыты
    private double r0 = 5;
    private double phi0 = Math.PI / 4;
    // радиус
    private double r = 1;

    public Circle(double r0, double PHI0, double a) {
        this.r0 = r0;
        this.phi0 = PHI0;
        r = a;
    }

    private boolean solve(double phi, double[] r) {
        double cos = Math.cos(phi0 - phi);
        //квадраты слогаемых для 1/4 дискт
        double a2 = this.r * this.r;
        double r02 = r0 * r0;
        double cos2 = cos * cos;
        double d = a2 + r02 * cos2 - r02;

        if (d < 0) {
            return false;
        }

        r[0] = r0 * cos - Math.sqrt(d);
        r[1] = r0 * cos + Math.sqrt(d);
        return true;
    }

    public Border createBorder() {
        return createBorder(DEFAULT_DELTA);
    }

    public Border createBorder(double delta) {
        double[] mesh = StoksUtil.createUniformMesh(0, Math.PI / 2, delta);
        DoubleArray neighborRadius = new DoubleArray(mesh.length);
        DoubleArray neighborAlpha = new DoubleArray(mesh.length);
        DoubleArray farRadius = new DoubleArray(mesh.length);
        DoubleArray farAlpha = new DoubleArray(mesh.length);

        double[] r0r1 = new double[2];
        for (int i = mesh.length - 1; i >= 0; --i) {
            double aMesh = mesh[i];
            if (solve(aMesh, r0r1)) {
                double min = StoksUtil.min(r0r1);
                double max = StoksUtil.max(r0r1);

                if (min < 0 || max < 0) {
                    throw new RuntimeException("Something went wrong. r0r1 =" + Arrays.toString(r0r1));
                }

                neighborRadius.add(min);
                neighborAlpha.add(aMesh);
                farRadius.add(max);
                farAlpha.add(aMesh);
            }
        }

        for (int i = farAlpha.length() - 1; i >= 0; --i) {
            neighborAlpha.add(farAlpha.get(i));
            neighborRadius.add(farRadius.get(i));
        }

        return new Border(neighborAlpha.toArray(), neighborRadius.toArray());
    }
}
