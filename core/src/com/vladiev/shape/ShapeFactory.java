package com.vladiev.shape;


import com.vladiev.model.Border;
import com.vladiev.util.StoksUtil;
import com.vladiev.util.Tool;
import org.la4j.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mazzachuses on 09.04.2016.
 */
public class ShapeFactory {

    public static Shape createCircle(double r, double x0, double y0, double stepMesh) {
        return createEllipse(r, r, x0, y0, 0, stepMesh);
    }

    public static Shape createCircle(double r, double x0, double y0, double stepMesh, double eps) {
        return createEllipse(r, r, x0, y0, 0, stepMesh, eps);
    }

    //http://compgraphics.info/2D/matrix_rotate.php
    public static Shape createEllipse(double a, double b,
                                      double x0, double y0,
                                      double tiltAngle, double stepMesh, double eps) {
        double[] mesh = StoksUtil.createUniformMesh(0, 2 * Math.PI, stepMesh);
        List<Vector> border = new ArrayList<>(mesh.length);
        for (double t : mesh) {
            double tempX = a * Math.cos(t);
            double tempY = b * Math.sin(t);

            double x = tempX * Math.cos(tiltAngle) - tempY * Math.sin(tiltAngle) + x0;
            double y = tempX * Math.sin(tiltAngle) + tempY * Math.cos(tiltAngle) + y0;
            border.add(Tool.createVector(x, y));
        }
        return new Shape(border, eps);
    }


    public static Shape createEllipse(double a, double b,
                                      double x0, double y0,
                                      double tiltAngle, double stepMesh) {
        return createEllipse(a, b, x0, y0, tiltAngle, stepMesh, Shape.DEFAULT_EPSILON);
    }

    //https://en.wikipedia.org/wiki/Deltoid_curve
    public static Shape createDeltoid(double a, double x0, double y0, double stepMesh) {
        double[] mesh = StoksUtil.createUniformMesh(0, 2 * Math.PI, stepMesh);
        List<Vector> border = new ArrayList<>(mesh.length);
        for (double t : mesh) {
            double tempX = 2 * a * Math.cos(t) + a * Math.cos(2 * t);
            double tempY = 2 * a * Math.sin(t) - a * Math.sin(2 * t);
            border.add(Tool.createVector(tempX, tempY));
        }
        return new Shape(border);
    }
}
