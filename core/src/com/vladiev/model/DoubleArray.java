package com.vladiev.model;

import java.util.Arrays;

/**
 * Created by mazzachuses on 06.06.15.
 */
public class DoubleArray {
    private double[] array;
    private int length;

    public DoubleArray(final int capacity) {
        array = new double[Math.max(capacity, 10)];
        this.length = 0;
    }

    public DoubleArray() {
        this(10);
    }

    public int length() {
        return length;
    }

    public double get(final int index) {
        if (index < 0 || index >= length)
            throw new IndexOutOfBoundsException();
        return array[index];
    }

    public void set(final int index, final double elem) {
        if (index < 0 || index >= length)
            throw new IndexOutOfBoundsException();
        array[index] = elem;
    }

    private void enlarge() {
        if (++length > array.length) {
            array = Arrays.copyOf(array, Math.max(2 * array.length, 10));
        }
    }

    public void add(double newElem) {
        enlarge();
        set(length - 1, newElem);
    }

    public void sort() {
        Arrays.sort(array, 0, length);
    }

    public void clear() {
        length = 0;
    }

    public double[] toArray() {
        return Arrays.copyOf(array, length);
    }
}
