package com.vladiev.model.gomoboric;

import com.vladiev.model.Model2;
import com.vladiev.shape.Shape;
import com.vladiev.shape.ShapeFactory;
import com.vladiev.util.StoksUtil;
import com.vladiev.util.Tool;
import org.apache.log4j.Logger;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mazzachuses on 10.04.16.
 */
public class GomoboricModelBase extends Model2 {
    private static final Logger LOG = Logger.getLogger(GomoboricModelBase.class);

    static final double TENSOR_FACTOR = 1;
    static final double COEFFICIENT_OF_VISCOSITY = 0.1;
    static final Vector GRAPHIC_OFFSET = Tool.createVector(300, 300);

    static final Shape EXTERNAL_SHAPE;
    static final List<Shape> INTERNAL_SHAPES;
    public static final int SCALE = 50;
    private static final double TIME_STEP = 0.00005;



    static {
        double aExternal = 2;
        double bExternal = 2;
        double x0 = 0;
        double y0 = 0;
        double tiltAngle = 0;
        double stepMesh = 0.001;
        double epsilon = 0.05;

        EXTERNAL_SHAPE = ShapeFactory.createEllipse(aExternal, bExternal, x0, y0, tiltAngle, stepMesh, epsilon);

        double r = 0.55;
        Shape internalShape = ShapeFactory.createEllipse(r, r, 0, 1, tiltAngle, stepMesh, epsilon);
        internalShape.markAsInternal();

        Vector e1 = new BasicVector(new double[]{1, 0});
        Vector e2 = new BasicVector(new double[]{Math.cos(Math.PI / 3), Math.sin(Math.PI / 3)});
        double a = .25;
        double b = .25;
        INTERNAL_SHAPES = new ArrayList<>();


        for (int m = -6; m < 6; ++m) {
            for (int n = -6; n < 6; ++n) {

                if (m == 0 && n == 0) {
                    continue;
                }
                Vector v = e1.multiply(m).add(e2.multiply(n)).multiply(.62);
                if (v.norm() + a >= aExternal) {
                    continue;
                }
                Shape ellipse = ShapeFactory.createEllipse(a, b, v.get(0), v.get(1), 0, epsilon);
                ellipse.setType(Shape.Type.INTERNAL);
                INTERNAL_SHAPES.add(ellipse);
            }
        }


    }

    public GomoboricModelBase(double tensionFactor, double coefficientOfViscosity, Shape externalShape, List<Shape> internalShapes, Vector graphicOffset) {
        super(tensionFactor, coefficientOfViscosity, externalShape, internalShapes, graphicOffset, SCALE);

    }

    public GomoboricModelBase(Shape externalShape, List<Shape> internalShapes) {
        super(TENSOR_FACTOR, COEFFICIENT_OF_VISCOSITY, externalShape, internalShapes, GRAPHIC_OFFSET, SCALE);
    }


    public GomoboricModelBase() {
        super(TENSOR_FACTOR, COEFFICIENT_OF_VISCOSITY, EXTERNAL_SHAPE, INTERNAL_SHAPES, GRAPHIC_OFFSET, SCALE);
        historyFileName = "history\\gomoboric\\historyFileName";
    }

    @Override
    protected void doMathCalculation() {
      /*  try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        Vector centerOfMass = countCenterOfMass().multiply(0);
        double pressure = getPressure();
        LOG.debug("pressure=" + pressure);
        process(centerOfMass, pressure, externalShape);
        internalShapes.stream().forEach(shape -> process(centerOfMass, pressure, shape));
        List<Shape> collect = internalShapes.stream().filter(it -> it.getAria() > 2.0E-5).collect(Collectors.toList());
        if (collect.isEmpty() && !internalShapes.isEmpty()) {
            //  setRunning(false);
            System.out.println("holes were dropped.");
        }
        internalShapes = collect;

    }

    private void process(Vector centerOfMass, double pressure, Shape shape) {
        List<Vector> velocities = countVelocities(centerOfMass, pressure, shape);
        shape.recountBorder(velocities, TIME_STEP);
    }

    private List<Vector> countVelocities(Vector centerOfMass, double pressure, Shape shape) {
        List<Vector> deckardBorder = shape.getDeckardBorder();
        List<Vector> normals = shape.getNormals();
        List<Vector> velocities = new ArrayList<>();
        for (int i = 0; i < deckardBorder.size(); ++i) {
            Vector normal = normals.get(i);
            Vector res = normal.multiply(tensionFactor);
            Vector fi = deckardBorder.get(i);
            fi = fi.subtract(centerOfMass);
            fi = fi.multiply(pressure);
            res = res.subtract(fi);
            res = res.multiply(1 / (2.0 * coefficientOfViscosity));
            velocities.add(res);
        }
        return velocities;
    }


    private Vector countCenterOfMass() {
        Vector centerOfMass = externalShape.getCenterOfMass();
        // centerOfMass = internalShapes.stream().map(Shape::getCenterOfMass).reduce(centerOfMass, Vector::add);
        //return centerOfMass.divide(internalShapes.size() + 1);
        return centerOfMass;
    }

    private double getPressure() {
        double perimeter = getPerimeter();

        double area = getArea();
        LOG.debug("area=" + area + ", perimeter=" + perimeter);
        return (tensionFactor * perimeter) / (2.0 * area);
    }

    private double getPerimeter() {
        double perimeter = externalShape.getPerimeter();
        double[] perimeters = internalShapes.stream().mapToDouble(Shape::getPerimeter).toArray();
        for (double x : perimeters) {
            perimeter += x;
        }
        return perimeter;
    }

    private double getArea() {
        double area = externalShape.getAria();
        area -= internalShapes.stream().mapToDouble(Shape::getAria).sum();
        return area;
    }

}
