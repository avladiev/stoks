package com.vladiev.model.gomoboric;

import com.vladiev.shape.Shape;
import com.vladiev.shape.ShapeFactory;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mazzachuses on 15.04.16.
 */
public class GomoboricDebug extends GomoboricModelBase {

    static final Shape EXTERNAL_SHAPE;
    static final List<Shape> INTERNAL_SHAPES;

    static {
        double aExternal = 160;
        double bExternal = 160;
        double x0 = 0;
        double y0 = 0;
        double tiltAngle = 0;
        double epsilon = 0.1;
        EXTERNAL_SHAPE = ShapeFactory.createEllipse(aExternal, bExternal, x0, y0, tiltAngle, epsilon);

        INTERNAL_SHAPES = new ArrayList<>();
        double a = 15;
        double b = 15;
        Shape ellipse = ShapeFactory.createEllipse(a, b, 0, 0, 0, epsilon);
        ellipse.setType(Shape.Type.INTERNAL);
        INTERNAL_SHAPES.add(ellipse);
    }

    public GomoboricDebug() {
        super(EXTERNAL_SHAPE, INTERNAL_SHAPES);
    }


}
