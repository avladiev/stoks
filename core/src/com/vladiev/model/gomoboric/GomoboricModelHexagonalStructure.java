package com.vladiev.model.gomoboric;

import com.vladiev.shape.Shape;
import com.vladiev.shape.ShapeFactory;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by mazzachuses on 15.04.16.
 */
public class GomoboricModelHexagonalStructure extends GomoboricModelBase {

    static final Shape EXTERNAL_SHAPE;
    static final List<Shape> INTERNAL_SHAPES;

    static {
        double aExternal = 100;
        double bExternal = 100;
        double x0 = 0;
        double y0 = 0;
        double tiltAngle = 0;
        double epsilon = 0.1;
        EXTERNAL_SHAPE = ShapeFactory.createEllipse(aExternal, bExternal, x0, y0, tiltAngle, epsilon);

        INTERNAL_SHAPES = new ArrayList<>();

        Vector e1 = new BasicVector(new double[]{1, 0});
        Vector e2 = new BasicVector(new double[]{Math.cos(Math.PI / 3), Math.sin(Math.PI / 3)});
        double a = 15;
        double b = 15;

        for (int m = -6; m < 6; ++m) {
            for (int n = -6; n < 6; ++n) {

                if (m == 0 && n == 0) {
                    continue;
                }

                Vector v = e1.multiply(m).add(e2.multiply(n)).multiply(30);
                if (v.norm() + a >= aExternal) {
                    continue;
                }

                Shape ellipse = ShapeFactory.createEllipse(a, b, v.get(0), v.get(1), 0, epsilon);
                ellipse.setType(Shape.Type.INTERNAL);
                INTERNAL_SHAPES.add(ellipse);
            }
        }
    }

    public GomoboricModelHexagonalStructure() {
        super(EXTERNAL_SHAPE, INTERNAL_SHAPES);
    }


}
