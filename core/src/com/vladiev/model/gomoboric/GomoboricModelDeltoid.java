package com.vladiev.model.gomoboric;

import com.vladiev.shape.Shape;
import com.vladiev.shape.ShapeFactory;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mazzachuses on 15.04.16.
 */
public class GomoboricModelDeltoid extends GomoboricModelBase{

    static final Shape EXTERNAL_SHAPE;
    static final List<Shape> INTERNAL_SHAPES;

    static {
        double aExternal = 30;
        double x0 = 0;
        double y0 = 0;
        double epsilon = 0.1;
        EXTERNAL_SHAPE = ShapeFactory.createDeltoid(aExternal,x0,y0,epsilon);

        Shape deltoid = ShapeFactory.createDeltoid(10, x0, y0, epsilon);
        deltoid.markAsInternal();
        INTERNAL_SHAPES = Arrays.asList(deltoid);
    }

    public GomoboricModelDeltoid() {
        super(EXTERNAL_SHAPE, INTERNAL_SHAPES);
    }
}
