package com.vladiev.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.google.gson.Gson;
import com.vladiev.shape.Shape;
import org.la4j.Vector;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.vladiev.util.Tool.getBagLogicVector;

/**
 * Created by mazzachuses on 10.04.2016.
 */
public class Model2 {
    protected final double tensionFactor;//G коэф пов напяжения

    protected final double coefficientOfViscosity; //коэф динамической вязкости
    protected final Shape externalShape;
    protected  List<Shape> internalShapes;

    //шаг эволюции - номер области- граница
    List<List<List<Vector>>> history = new ArrayList<>();
    int saveStep = 3000;

    //for graphics
    Vector graphicOffset; //x0y0
    double scale = 1;
    int stepOfEvolution = 0;
    boolean running = true;
    protected String historyFileName = "history\\historyFileName";


    public Model2(double tensionFactor,
                  double coefficientOfViscosity,
                  Shape externalShape, List<Shape> internalShapes,
                  Vector graphicOffset, double scale) {
        this.tensionFactor = tensionFactor;
        this.coefficientOfViscosity = coefficientOfViscosity;
        this.externalShape = externalShape;
        this.internalShapes = internalShapes;
        this.graphicOffset = graphicOffset;
        this.scale = scale;
        externalShape.alignDeckardBorder();

    }

    public void render(ShapeRenderer renderer) {

        if (isRunning()) {

            Gdx.app.log("StepOfEvolution", stepOfEvolution + "");
            saveConfigurationToHistory();
            doMathCalculation();
            stepOfEvolution++;
            if (stepOfEvolution % saveStep == 0) {
                saveHistory();
            }
          /*  if (Arrays.asList(1,  1000, 1499).contains(stepOfEvolution)) {
                setRunning(false);
            }*/
        }
        showAxis(renderer);
        showExternalShape(renderer);
        showInternalShapes(renderer);
    }

    private void saveHistory() {
        Gson gson = new Gson();
        String json = gson.toJson(history);
        try (FileWriter fileWriter = new FileWriter(historyFileName + stepOfEvolution);) {
            fileWriter.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void saveConfigurationToHistory() {
        List<Vector> deckardBorder = externalShape.getDeckardBorder();
        List<List<Vector>> borders = new ArrayList<>();
        borders.add(deckardBorder);
        internalShapes.forEach(shape -> borders.add(shape.getDeckardBorder()));
        history.add(borders);

    }

    protected void showInternalShapes(ShapeRenderer renderer) {
        renderer.begin(ShapeRenderer.ShapeType.Line);
        for (Shape shape : internalShapes) {
            showShape(renderer, shape);
        }
        renderer.end();
    }

    protected void showExternalShape(ShapeRenderer renderer) {
        showShape(renderer, externalShape);
        renderer.begin(ShapeRenderer.ShapeType.Line);
        float[] startPoints = externalShape.getStartPoints();
        if (startPoints != null) {
            renderer.setColor(Color.BLACK);
            renderer.polygon(startPoints);
        }
        renderer.end();
    }

    private void showShape(ShapeRenderer shapeRenderer, Shape shape) {
        boolean drawing = shapeRenderer.isDrawing();
        if (!drawing) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        }
        shapeRenderer.set(ShapeRenderer.ShapeType.Line);
        float[] libGdxFormat = shape.getCurrentPoints(graphicOffset, scale);

        shapeRenderer.setColor(Color.RED);
        shapeRenderer.polygon(libGdxFormat);

       /* shapeRenderer.setColor(Color.GREEN);
        shapeRenderer.circle(libGdxFormat[0], libGdxFormat[1], 1);*/

        /*//show normals
        List<Vector> normals = shape.getNormals();
        List<Vector> deckardBorder = shape.getDeckardBorder();
        shapeRenderer.setColor(Color.BLACK);
        for (int i = 0; i < normals.size(); ++i) {
            Vector point1 = deckardBorder.get(i).multiply(scale).add(graphicOffset);
            Vector point2 = normals.get(i).multiply(2.).add(point1);
            shapeRenderer.line(
                    getBagLogicVector(point1),
                    getBagLogicVector(point2));
        }
*/

        if (!drawing) {
            shapeRenderer.end();
        }

    }

    private void showAxis(ShapeRenderer renderer) {
        Gdx.gl.glLineWidth(1);
        renderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.setColor(Color.LIGHT_GRAY);

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        float y0 = (float) graphicOffset.get(0);
        float x0 = (float) graphicOffset.get(1);
        renderer.line(0, y0, width, y0);
        renderer.line(x0, 0, x0, height);
        for (float x = 0; x < width; x += 10) {
            int w = x % 100 == 0 ? 4 : 2;
            float y = y0 - w;
            float y2 = y0 + w;
            renderer.line(x, y, x, y2);
        }
        for (float y = 0; y < height; y += 10) {
            int w = y % 100 == 0 ? 4 : 2;
            float x = x0 - w;
            float x2 = x0 + w;
            renderer.line(x, y, x2, y);
        }
        renderer.end();
        Gdx.gl.glLineWidth(2);
    }

    protected void doMathCalculation() {

    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
