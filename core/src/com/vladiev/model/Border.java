package com.vladiev.model;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by mazzachuses on 06.06.15.
 */
public class Border {
    private final double[] firstCoordinates;
    private final double[] secondCoordinates;

    public Border(double[] firstCoordinates, double[] secondCoordinates) {
        this.firstCoordinates = firstCoordinates;
        this.secondCoordinates = secondCoordinates;
    }

    public double[] getSecondCoordinates() {
        return secondCoordinates;
    }

    public double[] getFirstCoordinates() {
        return firstCoordinates;
    }

    public String toString() {
        Border border = this;
        StringBuilder sb = new StringBuilder();
        double[] gamma = border.getFirstCoordinates();
        double[] radius = border.getSecondCoordinates();
        String collect = Arrays.stream(gamma).mapToObj(x -> x + "").collect(Collectors.joining(", "));
        // sb.append("firstCoordinates:\n");
        // sb.append("\nsecondCoordinates:\n");

        sb.append("x=[").append(collect).append("]");
        sb.append("\n");
        collect = Arrays.stream(radius).mapToObj(x -> x + "").collect(Collectors.joining(", "));
        sb.append("y=[").append(collect).append("]");
        return sb.toString();
    }


    public int length() {
        return firstCoordinates.length;
    }

    /**
     * @return array where every even element represents the horizontal part of a point, and the following element
     * representing the vertical part
     */
    public float[] getLibGdxFormat(double x0, double y0) {
        int len = 2 * firstCoordinates.length;
        float[] res = new float[len];
        int j = 0;
        for (int i = 0; i < len; i += 2) {
            res[i] = (float) (firstCoordinates[j] + x0);
            res[i + 1] = (float) (secondCoordinates[j] + y0);
            j++;
        }
        return res;
    }
}
