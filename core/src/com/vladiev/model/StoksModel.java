package com.vladiev.model;

import com.vladiev.shape.Shape;
import com.vladiev.shape.ShapeFactory;
import com.vladiev.util.HardStoksUtil;
import com.vladiev.util.StoksUtil;
import com.vladiev.util.Tool;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by mazzachuses on 10.04.16.
 */
public class StoksModel extends Model2 {
    private static final double TENSOR_FACTOR = 1;
    private static final double COEFFICIENT_OF_VISCOSITY = 0.1;
    private static final int NUMBER_OF_HARMONIC_FUNCTIONS = 1;
    private static final Vector GRAPHIC_OFFSET = Tool.createVector(300, 300);
    private static final double scale = 50;

    private static final Shape EXTERNAL_SHAPE;
    private static final List<Shape> INTERNAL_SHAPES;
    private static final double TIME_STEP = 0.00005;


    static {
        double aExternal = 2;
        double bExternal = 2;
        double x0 = 0;
        double y0 = 0;
        double tiltAngle = 0;
        double stepMesh = 0.001;
        double epsilon = 0.05;

        EXTERNAL_SHAPE = ShapeFactory.createEllipse(aExternal, bExternal, x0, y0, tiltAngle, stepMesh, epsilon);

        double r = 0.55;
        Shape internalShape = ShapeFactory.createEllipse(r, r, 0, 1, tiltAngle, stepMesh, epsilon);
        internalShape.markAsInternal();

        Vector e1 = new BasicVector(new double[]{1, 0});
        Vector e2 = new BasicVector(new double[]{Math.cos(Math.PI / 3), Math.sin(Math.PI / 3)});
        double a = .25;
        double b = .25;
        INTERNAL_SHAPES = new ArrayList<>();


        for (int m = -6; m < 6; ++m) {
            for (int n = -6; n < 6; ++n) {

                if (m == 0 && n == 0) {
                    continue;
                }
                Vector v = e1.multiply(m).add(e2.multiply(n)).multiply(.62);
                if (v.norm() + a >= aExternal) {
                    continue;
                }
                Shape ellipse = ShapeFactory.createEllipse(a, b, v.get(0), v.get(1), 0, epsilon);
                ellipse.setType(Shape.Type.INTERNAL);
                INTERNAL_SHAPES.add(ellipse);
            }
        }
    }

    final int numberOfHarmonicFunctions;

    public StoksModel() {
        super(TENSOR_FACTOR, COEFFICIENT_OF_VISCOSITY, EXTERNAL_SHAPE, INTERNAL_SHAPES, GRAPHIC_OFFSET, scale);
        this.numberOfHarmonicFunctions = NUMBER_OF_HARMONIC_FUNCTIONS;
        historyFileName = "history\\stoks\\historyFileName";
    }


    @Override
    protected void doMathCalculation() {
        /*List<Vector> velocity = StoksUtil.countVelocity(externalShape,
                tensionFactor, coefficientOfViscosity, numberOfHarmonicFunctions);
        externalShape.recountBorder(velocity, TIME_STEP);
        */
        System.out.println(externalShape.getDeckardBorder().size());
        Vector coefficientsOfPressure = HardStoksUtil.countCoefficientsOfPressure(externalShape, internalShapes, tensionFactor);
        List<Vector> innerPoints = internalShapes.stream().map(Shape::getCenterOfMass).collect(Collectors.toList());

        List<Double> rGammaX = externalShape.getDeckardBorder().stream().map(x -> x.get(0)).collect(Collectors.toList());
        List<Double> rGammaY = externalShape.getDeckardBorder().stream().map(x -> x.get(1)).collect(Collectors.toList());
        rGammaX.add(rGammaX.get(0));
        rGammaY.add(rGammaY.get(0));
        printMathematicaFormat(rGammaX, "RgammaX");
        printMathematicaFormat(rGammaY, "RgammaY");
        List<List<Double>> rGammaXInner = new ArrayList<>();
        List<List<Double>> rGammaYInner = new ArrayList<>();
        for (Shape shape : internalShapes) {
            List<Vector> deckardBorder = shape.getDeckardBorder();
            List<Double> rGammaXI = deckardBorder.stream().map(x -> x.get(0)).collect(Collectors.toList());
            rGammaXI.add(rGammaXI.get(0));
            List<Double> rGammaYI = deckardBorder.stream().map(x -> x.get(1)).collect(Collectors.toList());
            rGammaYI.add(rGammaYI.get(0));
            rGammaXInner.add(rGammaXI);
            rGammaYInner.add(rGammaYI);
        }
        printMathematicaFormat(rGammaXInner, "RgammaXInner");


        printMathematicaFormat(rGammaYInner, "RgammaYInner");


        List<Double> coefficientsOfPressureList = new ArrayList<>();
        for (double x : coefficientsOfPressure) {
            coefficientsOfPressureList.add(x);
        }

        printMathematicaFormat(coefficientsOfPressureList, "koefP");

        process(externalShape, coefficientsOfPressure, innerPoints);
        internalShapes.stream().forEach(shape -> process(shape, coefficientsOfPressure, innerPoints));
        List<Shape> collect = internalShapes.stream().filter(it -> it.getAria() > 2.0E-5).collect(Collectors.toList());
        if (collect.isEmpty() && !internalShapes.isEmpty()) {
          //  setRunning(false);
            System.out.println("holes were dropped.");
            System.out.println("SE:" + stepOfEvolution);
        }
        internalShapes = collect;

    }

    void printMathematicaFormat(List<?> list, String varName) {
        if (list.isEmpty()) {
            return;
        }
        String s;
        if (list.get(0) instanceof Double) {
            s = toMarhematicaFormat(list);
        } else {
            List<String> collect = list.stream().map((Function<Object, String>) o -> toMarhematicaFormat((List) o)).collect(Collectors.toList());
            s = String.join(",", collect);
            s = "{" + s + "}";
        }
        System.out.println(varName + "=" + s + "; ");
    }

    private String toMarhematicaFormat(List<?> list) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        List<String> collect = list.stream().map(s -> String.format("%.10f", s)).collect(Collectors.toList());

        String join = String.join("_", collect);
        join = join.replace(",", ".").replace("_", ",");
        sb.append(join).append("}");
        return sb.toString();
    }


    private void process(Shape shape, Vector coefficientsOfPressure, List<Vector> innerPoints) {
        List<Vector> velocities = HardStoksUtil.countVelocities(coefficientOfViscosity, tensionFactor, coefficientsOfPressure, shape, innerPoints);
        shape.recountBorder(velocities, TIME_STEP);

    }
}
