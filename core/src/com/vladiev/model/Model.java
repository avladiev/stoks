package com.vladiev.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.*;
import com.vladiev.shape.Ellipse;
import com.vladiev.util.GeometryUtil;
import com.vladiev.util.StoksUtil;
import com.vladiev.util.StoksUtil2;
import org.la4j.Vector;
import org.la4j.linear.GaussianSolver;
import org.la4j.linear.LinearSystemSolver;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.vector.dense.BasicVector;

import java.lang.StringBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by mazzachuses on 07.06.15.
 */
public class Model {
    private final int numberOfHarmonicFunctions;
    private final double tensionFactor;//G коэф пов напяжения
    private final double coefficientOfViscosity = 4; //коэф динамической вязкости
    private final Ellipse externalShape;
    private List<Vector> externalShapeNormals = Collections.emptyList();
    private List<List<Vector>> internalShapeNormals;

    private final List<Ellipse> innerShapes;

    private int stepOfEvolution = 0;


    float[] startExternalShape = null;
    int x0 = 300;
    int y0 = 300;

    public Model(double tensionFactor, int numberOfHarmonicFunctions,
                 Ellipse externalShape, List<Ellipse> innerShapes, int x0, int y0) {
        this.tensionFactor = tensionFactor;
        this.numberOfHarmonicFunctions = numberOfHarmonicFunctions;
        this.externalShape = externalShape;
        this.innerShapes = innerShapes;
        this.internalShapeNormals = new ArrayList<>(innerShapes.size());
        for (int i = 0; i < innerShapes.size(); ++i) {
            internalShapeNormals.add(i, Collections.emptyList());
        }
        this.x0 = x0;
        this.y0 = y0;
    }

    public void render(ShapeRenderer renderer, boolean isRunning) {
        showExternalShape(renderer);
        showInternalShapes(renderer);
        if (isRunning) {
            stepOfEvolution++;
            Gdx.app.log("StepOfEvolution", stepOfEvolution + "");
            doMathCalculationGomoboric();

        }
    }


    private void showExternalShape(ShapeRenderer renderer) {
        Ellipse ellipse = externalShape;

        Border deckardBorder = ellipse.getDeckardBorder();
        float[] libGdxFormat = deckardBorder.getLibGdxFormat(x0, y0);
        if (startExternalShape == null) {
            startExternalShape = libGdxFormat;
        }

        renderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.setColor(1, 0, 0, 0);
        renderer.polygon(libGdxFormat);
        renderer.setColor(0, 0, 1, 0);
        renderer.polygon(startExternalShape);

        renderer.setColor(0, 1, 0, 0);
        renderer.circle(libGdxFormat[0], libGdxFormat[1], 2);
        renderer.circle(libGdxFormat[0], libGdxFormat[1], 0.1f);
        renderer.end();

        showNormals(renderer, externalShapeNormals, deckardBorder);


    }

    private List<Double> r = new ArrayList<>();
    private List<Double> r1 = new ArrayList<>();

    private List<Double> a = new ArrayList<>();

    private void showInternalShapes(ShapeRenderer renderer) {
        Vector a1 = null;
        Vector a2 = null;


        for (int i = 0; i < innerShapes.size(); ++i) {
            Ellipse ellipse = innerShapes.get(i);
            Border deckardBorder = ellipse.getDeckardBorder();
            float[] libGdxFormat = deckardBorder.getLibGdxFormat(x0, y0);

            renderer.begin(ShapeRenderer.ShapeType.Line);
            renderer.setColor(0, 0, 0, 0);
            renderer.polygon(libGdxFormat);
            renderer.setColor(1, 0, 0, 0);
            renderer.circle(libGdxFormat[0], libGdxFormat[1], .2f);

            renderer.setColor(0, 1, 0, 0);
            renderer.circle(libGdxFormat[2], libGdxFormat[3], .2f);

            renderer.setColor(0, 0, 1, 0);
            renderer.circle(libGdxFormat[4], libGdxFormat[5],.2f);



            renderer.end();

           /* renderer.begin(ShapeRenderer.ShapeType.Filled);
            Vector centerOfMass = GeometryUtil.centerOfMass(deckardBorder);
            float x = (float) centerOfMass.get(0) + x0;
            float y = (float) centerOfMass.get(1) + y0;
            renderer.circle(x, y, 1);
            renderer.end()*/;
            showNormals(renderer, internalShapeNormals.get(i), deckardBorder);
/*
            if (i == 0) {
                r.add(centerOfMass.norm());
                a1 = centerOfMass;
            }
            if (i == innerShapes.size() - 1) {
                r1.add(centerOfMass.norm());
            }
            if (i == 29) {
                a2 = centerOfMass;
            }*/
        }

/*//        a.add(a2.subtract(a1).norm());
        if (stepOfEvolution == 433) {
            List<String> collect = r1.stream().map(x -> String.format("%.4f", x)).collect(Collectors.toList());
            String join = String.join("_", collect);
            join = join.replace(",", ".").replace("_", ",");
            System.out.println("r1=[" + join + "]");

            collect = r.stream().map(x -> String.format("%.4f", x)).collect(Collectors.toList());
            join = String.join("_", collect);
            join = join.replace(",", ".").replace("_", ",");
            System.out.println("r=[" + join + "]");

            collect = a.stream().map(x -> String.format("%.4f", x)).collect(Collectors.toList());
            join = String.join("_", collect);
            join = join.replace(",", ".").replace("_", ",");
            System.out.println("a=[" + join + "]");

            System.exit(1);
        }*/
    }


    private Vector2 getBagLogicVector(Vector vector) {
        return new Vector2((float) vector.get(0),
                (float) vector.get(1));
    }

    //left relative to the array
    private Vector getLeft(Border border, int index) {
        int resIndex = -1;
        if (index > 0) {
            resIndex = index - 1;
        } else {
            resIndex = border.length() - 1;
        }
        return getPoint(border, resIndex);
    }

    private Vector getPoint(Border border, int resIndex) {
        double x = border.getFirstCoordinates()[resIndex];
        double y = border.getSecondCoordinates()[resIndex];
        return new BasicVector(new double[]{x, y});
    }

    //right relative to the array
    private Vector getRight(Border border, int index) {
        int resIndex = -1;
        if (index < border.length() - 1) {
            resIndex = index + 1;
        } else {
            resIndex = 0;
        }
        return getPoint(border, resIndex);
    }

    //гомоборическое приближение
    private void doMathCalculationGomoboric() {
        //count velocity
        //todo!!! impl for internall
        Vector centerOfMass = GeometryUtil.centerOfMass(externalShape.getDeckardBorder());

        double pressure = getPressure();
        countExternal(centerOfMass, pressure);
        countInternal(centerOfMass, pressure);

    }


    private void countInternal(Vector centerOfMass, double pressure) {
        //internalShapeNormals = normals;
        for (int i = 0; i < innerShapes.size(); ++i) {
            Ellipse innerShape = innerShapes.get(i);
            Border deckardBorder = innerShape.getDeckardBorder();
            List<Vector> normals = countInternalNormal(deckardBorder);
            internalShapeNormals.set(i, normals);
            List<Vector> velocities = countVelocities(centerOfMass, pressure, deckardBorder, normals);
            innerShape.recountBorder(velocities, 1);
        }
    }

    private void countExternal(Vector centerOfMass, double pressure) {
        Border deckardBorder = externalShape.getDeckardBorder();

        List<Vector> normals = countNormals(deckardBorder);
        externalShapeNormals = normals;

        List<Vector> velocities = countVelocities(centerOfMass, pressure, deckardBorder, normals);

        externalShape.recountBorder(velocities, .1);

    }

    private List<Vector> countVelocities(Vector centerOfMass, double pressure, Border deckardBorder, List<Vector> normals) {
        List<Vector> velocities = new ArrayList<>();
        for (int i = 0; i < deckardBorder.length(); ++i) {
            Vector normal = normals.get(i);
            Vector res = normal.multiply(tensionFactor);
            Vector fi = getPoint(deckardBorder, i);
            fi = fi.subtract(centerOfMass);
            fi = fi.multiply(pressure);
            res = res.subtract(fi);
            res = res.multiply(1 / (2.0 * coefficientOfViscosity));
            velocities.add(res);
        }
        return velocities;
    }

    private double getPressure() {
        double perimeter = getPerimeter();
        double area = getArea();
        double pressure = (tensionFactor * perimeter) / (2.0 * area);
        return pressure;
    }

    private double getArea() {
        double area = GeometryUtil.countArea(externalShape.getDeckardBorder());
        for (Ellipse innerShape : innerShapes) {
            area -= GeometryUtil.countArea(innerShape.getDeckardBorder());
        }
        System.out.println("area:" + area);
        return area;
    }

    private double getPerimeter() {
        double perimeter = GeometryUtil.countPerimeter(externalShape.getDeckardBorder());
        for (Ellipse innerShape : innerShapes) {
            perimeter += GeometryUtil.countPerimeter(innerShape.getDeckardBorder());
        }
        System.out.println("perimeter:" + perimeter);
        return perimeter;
    }


    private void doMathCalculation(ShapeRenderer renderer) {

        //count coefficients of pressure
        Border polarBorder = externalShape.getPolarBorder();
        double[][] A = new double[numberOfHarmonicFunctions][numberOfHarmonicFunctions];
        double[] B = new double[numberOfHarmonicFunctions];
        for (int m = 0; m < A.length; ++m) {
            for (int k = 0; k < A[m].length; ++k) {
                A[m][k] = StoksUtil.elementOfA(m, k, polarBorder);
            }
            B[m] = (tensionFactor / 2) * StoksUtil.elementOfB(m, polarBorder);
        }
        Basic2DMatrix a = new Basic2DMatrix(A);
        // Gdx.app.debug("matrix", "\n" + a);
        LinearSystemSolver linearSystemSolver = new GaussianSolver(a);
        BasicVector b = new BasicVector(B);
        Vector coefficientsOfPressure = linearSystemSolver.solve(b);
        Gdx.app.debug("coefficients of pressure", coefficientsOfPressure + "");


        //count pressure
        double[] angels = polarBorder.getFirstCoordinates();
        double[] radii = polarBorder.getSecondCoordinates();
        Border deckardBorder = externalShape.getDeckardBorder();

        for (int i = 0; i < polarBorder.length(); ++i) {
            double[] psyValues = StoksUtil.countPsy(radii[i], angels[i], numberOfHarmonicFunctions);
            BasicVector psy = new BasicVector(psyValues);
            double pressure = coefficientsOfPressure.innerProduct(psy);
            System.out.println("point " + i + ". " + getPoint(deckardBorder, i) + ". pressure=" + pressure);
        }


        List<Vector> normals = countNormals(deckardBorder);
        showNormals(renderer, normals, deckardBorder);

        //count velocity

        List<Vector> velocities = new ArrayList<>();
        for (int i = 0; i < polarBorder.length(); ++i) {
            Vector normal = normals.get(i);
            Vector res = normal.multiply(tensionFactor);

            Vector sum = new BasicVector(2);
            for (int k = 0; k < coefficientsOfPressure.length(); ++k) {
                Vector heeK = new BasicVector(StoksUtil2.heeK(k, radii[i], angels[i]));
                if (i == 2 || i == 6) {
                    System.out.println("k=" + k + " " + heeK);
                }
                sum = sum.add(heeK.multiply(coefficientsOfPressure.get(k)));
            }
            if (i == 2 || i == 6) {
                System.out.println(sum);
            }
            res = res.subtract(sum);
            res = res.multiply(1 / (2.0 * coefficientOfViscosity));
            velocities.add(res);
        }

        externalShape.recountBorder(velocities, 0.01);

    }

    private List<Vector> countInternalNormal(Border decardBorder) {
        List<Vector> vectors = countNormals(decardBorder);
        List<Vector> collect = vectors.stream().map(new Function<Vector, Vector>() {
            @Override
            public Vector apply(Vector vector) {
                return vector.multiply(-1.0);
            }
        }).collect(Collectors.toList());
        return collect;
    }

    //count normals
    private List<Vector> countNormals(Border deckardBorder) {
        List<Vector> normals = new ArrayList<>();
        for (int i = 0; i < deckardBorder.length(); ++i) {
            Vector left = getLeft(deckardBorder, i);
            Vector right = getRight(deckardBorder, i);
            Vector center = getPoint(deckardBorder, i);
            Vector normal = GeometryUtil.countNormal(center, left, right);
            normal = GeometryUtil.countExternalNormal(normal, left, center);
            normals.add(normal);
        }
        return normals;
    }

    private void showNormals(ShapeRenderer renderer, List<Vector> normals, Border deckardBorder) {
        //show normals
        Vector x0y0 = new BasicVector(new double[]{x0, y0});
        renderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.setColor(0, 0, 0, 0);
        for (int i = 0; i < normals.size(); ++i) {
            Vector point1 = getPoint(deckardBorder, i).add(x0y0);
            Vector point2 = normals.get(i).multiply(2).add(point1);
            renderer.line(getBagLogicVector(point1), getBagLogicVector(point2));
        }
        renderer.end();
    }


}
