package com.vladiev.player;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.vladiev.model.Model2;
import com.vladiev.model.gomoboric.GomoboricModelBase;
import com.vladiev.util.ScreenshotSaver;

import java.io.IOException;

/**
 * Created by vladia on 5/13/2016.
 */
public class PlayerApplication extends ApplicationAdapter {
    private static final float SCALE = 1f;
    OrthographicCamera camera;
    SpriteBatch batch;
    ShapeRenderer renderer;
    float width;
    float height;
    Player player ;



    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();
        Gdx.app.log("init", "width=" + width + ", height=" + height);
        player = new Player();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, width / SCALE, height / SCALE);

        batch = new SpriteBatch();
        batch.setProjectionMatrix(camera.combined);
        renderer = new ShapeRenderer();
        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.SPACE) {
                 player.setRunning(!player.isRunning());
                }
                if (keycode == Input.Keys.Z) {
                    try {
                        ScreenshotSaver.saveScreenshot("xxxx");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return super.keyDown(keycode);
            }
        });

    }


    private void handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
            camera.zoom += camera.zoom * 0.02f;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            camera.zoom -= camera.zoom * 0.02f;
            if (camera.zoom < 0) {
                camera.zoom = 0;
            }
        }
        float x = 0;
        float y = 0;
        float velocity = 4 * camera.zoom;
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            x = -velocity;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            x = velocity;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            y = -velocity;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            y = velocity;
        }

        camera.translate(x, y);

    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        handleInput();
        camera.update();
        renderer.setProjectionMatrix(camera.combined);
        player.render(renderer);


    }


}
