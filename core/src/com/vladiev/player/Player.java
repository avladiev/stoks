package com.vladiev.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.google.gson.Gson;
import com.vladiev.shape.Shape;
import com.vladiev.util.Tool;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Player {
    //for graphics
    Vector graphicOffset = Tool.createVector(300, 300); //x0y0
    double scale = 50;
    int stepOfEvolution = 0;
    boolean running = true;

    String stoksHistoryFileName = "history\\stoks\\hexagonal\\historyFileName3000";
    //шаг эволюции - номер области- граница
    Vector[][][] stoksHistory;
    List<Shape> stoksCurrentShapes = new ArrayList<Shape>();
    String gomoboricHistoryFileName = "history\\gomoboric\\hexagonal\\historyFileName3000";
    //шаг эволюции - номер области- граница
    Vector[][][] gomoboricHistory;
    List<Shape> gomoboricCurrentShapes = new ArrayList<>();

    Statistics stoksStatistics = new Statistics();
    Statistics gomoboricStatistics = new Statistics();


    public Player() {
        try (FileReader stoksReader = new FileReader(stoksHistoryFileName);
             FileReader gomoboricReader = new FileReader(gomoboricHistoryFileName);
        ) {
            stoksHistory = new Gson().fromJson(stoksReader, BasicVector[][][].class);
            gomoboricHistory = new Gson().fromJson(gomoboricReader, BasicVector[][][].class);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void render(ShapeRenderer renderer) {
        if (isRunning() && stepOfEvolution < stoksHistory.length) {
            Gdx.app.log("StepOfEvolution", stepOfEvolution + "");
            setUpCurrentState(stoksHistory[stepOfEvolution], stoksCurrentShapes);
            setUpCurrentState(gomoboricHistory[stepOfEvolution], gomoboricCurrentShapes);
            stoksStatistics.countStatistics(stoksCurrentShapes, stepOfEvolution);
            gomoboricStatistics.countStatistics(gomoboricCurrentShapes, stepOfEvolution);
            stepOfEvolution++;
            if (stepOfEvolution == 760){
                System.out.println("stoks statistics:");
                stoksStatistics.printStatistics();
                System.out.println("gomoboric statistics:");
                gomoboricStatistics.printStatistics();
            }
          /*  if (Arrays.asList(1, 300, 745).contains(stepOfEvolution)) {
                setRunning(false);
            }*/
        }
        showAxis(renderer);
        showShapes(renderer);
    }


    private void doSomeThing() {


    }

    Shape firstExternalShape;
    private void setUpCurrentState(Vector[][] vectors, List<Shape> currentShapes) {
        currentShapes.clear();
        for (Vector[] border : vectors) {
            Shape e = new Shape(Arrays.asList(border), true);
            currentShapes.add(e);
            if (firstExternalShape == null){
                firstExternalShape = e;
            }
        }
    }


    protected void showShapes(ShapeRenderer renderer) {
        Gdx.gl.glLineWidth(2);
        renderer.begin(ShapeRenderer.ShapeType.Line);

        showShape(renderer,Color.BLACK,firstExternalShape);
        stoksCurrentShapes.stream().forEach(shape -> showShape(renderer, Color.RED, shape));
        gomoboricCurrentShapes.stream().forEach(shape -> showShape(renderer, Color.BLUE, shape));
        renderer.end();
        Gdx.gl.glLineWidth(1);
    }


    private void showShape(ShapeRenderer shapeRenderer, Color color, Shape shape) {
        boolean drawing = shapeRenderer.isDrawing();
        if (!drawing) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        }
        shapeRenderer.set(ShapeRenderer.ShapeType.Line);

        float[] libGdxFormat = shape.getCurrentPoints(graphicOffset, scale);

        shapeRenderer.setColor(color);
        shapeRenderer.polygon(libGdxFormat);

        if (!drawing) {
            shapeRenderer.end();
        }

    }

    private void showAxis(ShapeRenderer renderer) {
        renderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.setColor(Color.LIGHT_GRAY);

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        float y0 = (float) graphicOffset.get(0);
        float x0 = (float) graphicOffset.get(1);
        renderer.line(0, y0, width, y0);
        renderer.line(x0, 0, x0, height);
        for (float x = 0; x < width; x += 10) {
            int w = x % 100 == 0 ? 4 : 2;
            float y = y0 - w;
            float y2 = y0 + w;
            renderer.line(x, y, x, y2);
        }
        for (float y = 0; y < height; y += 10) {
            int w = y % 100 == 0 ? 4 : 2;
            float x = x0 - w;
            float x2 = x0 + w;
            renderer.line(x, y, x2, y);
        }
        renderer.end();
    }

    protected void doMathCalculation() {

    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }


}
