package com.vladiev.player;


import com.vladiev.shape.Shape;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Statistics {

    private Double lInf = null;
    private Double area = null;
    private Double l0 = null;
    private List<Double> lWithoutValue = new ArrayList<>();

    double tensorFactor = 1;
    double coefficientOfViscosity = 0.1;
    List<Double> tWithoutValue = new ArrayList<>();
    List<Double> lTheoretical = new ArrayList<>();

    private static final double TIME_STEP = 0.00005;
    Integer m;

    public void countStatistics(List<Shape> shapes, int stepOfEvolution) {
        double area = getArea(shapes);
        if (lInf == null) {
            double v = Math.PI * area;
            lInf = 2 * Math.sqrt(v);
            this.area = area;
        }

        double perimeter = getPerimeter(shapes);

        double lWithoutVal = perimeter / lInf;
        lWithoutValue.add(lWithoutVal);

        double t = stepOfEvolution * TIME_STEP;
        double tWithputVal = tensorFactor * t / (2 * coefficientOfViscosity) * Math.sqrt(Math.PI / this.area);
        tWithoutValue.add(tWithputVal);
        if (l0 == null) {
            l0 = lWithoutVal;
        }

        if (m == null) {
            m = shapes.size() - 1;
        }
        double v;
        if (m == 1) {
            v = l0 / (1 + l0 * tWithputVal);
            lTheoretical.add(v);
        }
        if (m >= 2) {
            double sqrt = Math.sqrt(m - 1);
            double tmp = sqrt;
            tmp *= l0 - sqrt * Math.tan(tWithputVal * sqrt);
            v = tmp / (sqrt + l0 * Math.tan(tWithputVal * sqrt));
            lTheoretical.add(v);
        }

    }

    public void printStatistics() {
        /*print(tWithoutValue, "t");
        print(lWithoutValue, "lWithoutVal");
        print(lTheoretical, "lTheoretical");
        */
        System.out.println("======================");
        printForMathematica(tWithoutValue, lWithoutValue, "l");
        printForMathematica(tWithoutValue, lTheoretical, "lT");

    }

    public void printForMathematica(List<Double> t, List<Double> l, String varName) {
        List<String> tF = t.stream().map(x -> String.format("%.8f", x)).map(x -> x.replace(",", ".")).collect(Collectors.toList());
        List<String> lF = l.stream().map(x -> String.format("%.8f", x)).map(x -> x.replace(",", ".")).collect(Collectors.toList());
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        String delimiter = "";
        for (int i = 0; i < l.size(); ++i) {
            sb.append(delimiter).append("{").append(tF.get(i)).append(",").append(lF.get(i)).append("}");
            delimiter = ",";

        }
        sb.append("};");
        System.out.println(varName + "=" + sb);
    }

    private void print(List<Double> doubles, String varName) {
        List<String> collect = doubles.stream().map(x -> String.format("%.4f", x)).collect(Collectors.toList());
        String join = String.join("_", collect);
        join = join.replace(",", ".").replace("_", ",");
        System.out.println(varName + "=[" + join + "];");
    }

    private double getArea(List<Shape> shapes) {
        double area = shapes.get(0).getAria();
        area -= shapes.stream().skip(1).mapToDouble(Shape::getAria).sum();
        return area;
    }

    private double getPerimeter(List<Shape> shapes) {
        double perimeter = shapes.get(0).getPerimeter();
        perimeter += shapes.stream().skip(1).mapToDouble(Shape::getPerimeter).sum();
        return perimeter;
    }
}
