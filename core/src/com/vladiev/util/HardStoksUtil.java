package com.vladiev.util;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vladiev.shape.Shape;
import flanagan.integration.IntegralFunction;
import flanagan.integration.Integration;
import org.apache.log4j.Logger;
import org.la4j.Vector;
import org.la4j.linear.GaussianSolver;
import org.la4j.linear.LinearSystemSolver;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.ToDoubleFunction;

/**
 * Created by mazzachuses on 24.04.16.
 */
//instead stocks utill
public class HardStoksUtil {
    private static final Logger LOG = Logger.getLogger(HardStoksUtil.class);

    private static final int NUMBER_OF_GARMONIC_FUNCTIONS = 18;


    interface PsyFunction {
        double apply(double x, double y);
    }

    interface HeeFunction {
        Vector apply(Vector z);
    }

    static class FirstPsyFunction implements PsyFunction {
        int k;

        public FirstPsyFunction(int k) {
            this.k = k;
        }

        @Override
        public double apply(double x, double y) {
            Vector polar = GeometryUtil.toPolar(Tool.createVector(x, y));
            return StoksUtil.psyK(k, polar.get(1), polar.get(0));
        }
    }


    static final List<PsyFunction> FIRST_PSY_FUNCTIONS = new ArrayList<>();
    static final List<HeeFunction> FIRST_HEE_FUNCTIONS = new ArrayList<>();

    static {
        for (int i = 0; i < NUMBER_OF_GARMONIC_FUNCTIONS; ++i) {
            FIRST_PSY_FUNCTIONS.add(new FirstPsyFunction(i));
            int k = i;
            FIRST_HEE_FUNCTIONS.add((z) -> {
                        Vector polar = GeometryUtil.toPolar(z);
                        double[] doubles = StoksUtil2.heeK(k, polar.get(1), polar.get(0));
                        return new BasicVector(doubles);
                    }
            );
        }


    }

    private static Coordinate vectorToCoordinate(Vector vector) {
        return new Coordinate(vector.get(0), vector.get(1));
    }

    static ExecutorService executorService = Executors.newFixedThreadPool(7);

    public static double elementOfA(int m, int k, Shape externalShape, List<Shape> internalShapes, List<PsyFunction> psyFunctions) {
        List<Vector> polarBorder = externalShape.getPolarBorder();
        if (m < NUMBER_OF_GARMONIC_FUNCTIONS && k < NUMBER_OF_GARMONIC_FUNCTIONS) {
            double elementOfAExternalShape = StoksUtil.elementOfA(m, k, polarBorder);
            double sum = internalShapes.stream().map(Shape::getPolarBorder).mapToDouble(value -> StoksUtil.elementOfA(m, k, value)).sum();
            LOG.debug(Thread.currentThread().getId() + ",k=" + k + ",m=" + m + ", got answer fast ");
            return elementOfAExternalShape - sum;
        }

        List<Vector> deckardBorder = externalShape.getDeckardBorder();
        Vector[] externalRectangle = GeometryUtil.getExternalRectangle(deckardBorder);
        Vector p1 = externalRectangle[0];
        Vector p2 = externalRectangle[1];

        double x1 = p1.get(0);
        double x2 = p2.get(0);

        double y1 = p1.get(1);
        double y2 = p2.get(1);

        // create a factory using default values (e.g. floating precision)
        GeometryFactory fact = new GeometryFactory();
        Coordinate[] coordinates = new Coordinate[deckardBorder.size() + 1];
        for (int i = 0; i < deckardBorder.size(); ++i) {
            coordinates[i] = vectorToCoordinate(deckardBorder.get(i));
        }
        coordinates[deckardBorder.size()] = vectorToCoordinate(deckardBorder.get(0));
        LinearRing linearRing = fact.createLinearRing(coordinates);
        Polygon polygon = fact.createPolygon(linearRing, null);
        IntegralFunction function = x -> {
            IntegralFunction function2 = y -> {
                Point point = fact.createPoint(new Coordinate(x, y));
                if (point.within(polygon)) {
                    PsyFunction psyFunctionK = psyFunctions.get(k);
                    PsyFunction psyFunctionM = psyFunctions.get(m);
                    return psyFunctionK.apply(x, y) * psyFunctionM.apply(x, y);
                }
                return 0;
            };
            return Integration.trapezium(function2, y1, y2, 1000);
        };
        LOG.debug(Thread.currentThread().getId() + ",k=" + k + ",m=" + m + ", start integration");
        long l = System.currentTimeMillis();
        double trapezium = Integration.trapezium(function, x1, x2, 1000);
        LOG.debug(Thread.currentThread().getId() + ",k=" + k + ",m=" + m + ", answer is " + trapezium + ", time is " + (System.currentTimeMillis() - l));
        return trapezium;
    }

    public static double elementOfB(PsyFunction psyFunction, Shape externalShape, List<Shape> internalShapes) {
        double elementOfBExternalShape = elementOfB(externalShape, psyFunction);
        double sum = internalShapes.stream().mapToDouble(value -> elementOfB(value, psyFunction)).sum();
        return elementOfBExternalShape + sum;
    }


    public static double elementOfB(Shape shape, PsyFunction psyFunction) {
        List<Vector> deckardBorder = shape.getDeckardBorder();
        double sum = 0;
        for (int i = 0; i < deckardBorder.size(); ++i) {
            Vector point = deckardBorder.get(i);
            double psyK = psyFunction.apply(point.get(0), point.get(1));
            Vector delta = Tool.delta(deckardBorder, i);
            double dX = delta.get(0);
            double dX2 = dX * dX;

            double dY = delta.get(1);
            double dY2 = dY * dY;

            sum += psyK * Math.sqrt(dX2 + dY2);
        }
        return sum;
    }

    public static Vector countCoefficientsOfPressure(Shape externalShape, List<Shape> internalShapes, double tensionFactor) {
        LOG.debug("countCoefficientsOfPressure was called..");
        List<PsyFunction> psyFunctions = new ArrayList<>(FIRST_PSY_FUNCTIONS);
   /*     for (Shape s : internalShapes) {
            Vector centerOfMass = s.getCenterOfMass();

            for (int n = 3; n < NUMBER_OF_GARMONIC_FUNCTIONS + 3; ++n) {
                int k = n;
                PsyFunction psyFunction = (x, y) -> {
                    Vector z = Tool.createVector(x, y);
                    z = z.subtract(centerOfMass);
                    z = GeometryUtil.toPolar(z);
                    return psy(k, z.get(1), z.get(0));
                };
                psyFunctions.add(psyFunction);
            }
        }*/


        int numberOfHarmonicFunctions = psyFunctions.size();
        double[][] A = new double[numberOfHarmonicFunctions][numberOfHarmonicFunctions];
        double[] B = new double[numberOfHarmonicFunctions];
        Future<Double>[][] futures = new Future[numberOfHarmonicFunctions][];


        for (int m = 0; m < A.length; ++m) {
            futures[m] = new Future[m + 1];
            for (int k = 0; k <= m; ++k) {
                final int tmpM = m;
                final int tmpK = k;
                LOG.debug("task for [" + m + "," + k + "] is being submitted..");
                futures[m][k] = executorService.submit(() -> elementOfA(tmpM, tmpK, externalShape, internalShapes, psyFunctions));
            }
            B[m] = (tensionFactor / 2.) * elementOfB(psyFunctions.get(m), externalShape, internalShapes);
        }

        for (int m = 0; m < A.length; ++m) {
            for (int k = 0; k <= m; ++k) {
                try {
                    LOG.debug("waiting  for [" + m + "," + k + "] ");
                    double v = futures[m][k].get();
                    A[m][k] = v;
                    if (m != k) {
                        A[k][m] = v;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }

        Basic2DMatrix a = new Basic2DMatrix(A);
        LOG.debug("Matrix A:\n" + a);
        LinearSystemSolver linearSystemSolver = new GaussianSolver(a);
        BasicVector b = new BasicVector(B);
        LOG.debug("Vector b:\n" + b);
        Vector coefficientsOfPressure = linearSystemSolver.solve(b);
        LOG.debug("coefficients of pressure :\n" + coefficientsOfPressure);
        return coefficientsOfPressure;
    }


    static double psy(int k, double radius, double alpha) {
        if (k % 2 == 0) {
            int n = -k / 2;
            return Math.pow(radius, n) * Math.sin(n * alpha);
        }
        int n = -(k / 2 + 1);
        return Math.pow(radius, n) * Math.cos(n * alpha);
    }

    //z in decard
    static Vector hee(int k, Vector z, Vector innerPoint) {
        z = z.subtract(innerPoint);
        z = GeometryUtil.toPolar(z);
        double radius = z.get(1);
        double angel = z.get(0);

        if (k % 2 == 0) {
            int p = -k / 2 + 1;
            double first = Math.pow(radius, p) / p;
            double real = Math.sin(p * angel);
            double img = -Math.cos(p * angel);
            return Tool.createVector(real, img).multiply(first);
        }


        int p = -k;
        double first = Math.pow(radius, p) / p;
        double real = Math.cos(p * angel);
        double img = Math.sin((p * angel));
        return Tool.createVector(real, img).multiply(first);
    }


    public static List<Vector> countVelocities(double coefficientOfViscosity, double tensionFactor,
                                               Vector coefficientsOfPressure, Shape shape, List<Vector> innerPoints) {
        List<Vector> velocities = new ArrayList<>(shape.getDeckardBorder().size());
        List<Vector> normals = shape.getNormals();
        List<Vector> deckardBorder = shape.getDeckardBorder();
        List<HeeFunction> heeFunctions = new ArrayList<>(FIRST_HEE_FUNCTIONS);
/*
        for (Vector p : innerPoints) {
            for (int n = 3; n < NUMBER_OF_GARMONIC_FUNCTIONS + 3; ++n) {
                int k = n;
                heeFunctions.add((z) -> {
                    return hee(k, z, p);
                });
            }
        }*/
        for (int i = 0; i < deckardBorder.size(); ++i) {
            Vector normal = normals.get(i);
            Vector res = normal.multiply(tensionFactor);
            Vector sum = new BasicVector(2);
            Vector point = deckardBorder.get(i);


            List<Vector> heeKList = new ArrayList<>();
            for (int k = 0; k < coefficientsOfPressure.length(); ++k) {
                Vector heeK = heeFunctions.get(k).apply(point);
                heeKList.add(heeK);
                sum = sum.add(heeK.multiply(coefficientsOfPressure.get(k)));
            }

            res = res.subtract(sum);
            res = res.multiply(1. / (2. * coefficientOfViscosity));
            velocities.add(res);
        }
        return velocities;
    }


}
