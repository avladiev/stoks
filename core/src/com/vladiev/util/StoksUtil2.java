package com.vladiev.util;

/**
 * Created by mazzachuses on 12.03.16.
 */
//it contains methods for count hee function
public class StoksUtil2 {

    /**
     * Hee is a complex function
     *
     * @param k      number of hee
     * @param radius
     * @param alpha
     * @return
     */
    public static double[] heeK(int k, double radius, double alpha) {
        double[] result = new double[2];
        if (k == 0) {
            return heeKForZero(k, radius, alpha, result);
        }
        return k % 2 == 0 ? heeKForEven(k, radius, alpha, result) : heeKForOdd(k, radius, alpha, result);

    }

    private static double[] heeKForZero(int k, double radius, double alpha, double[] result) {
        result[0] = radius * Math.cos(alpha);
        result[1] = radius * Math.sin(alpha);
        return result;
    }

    private static double[] heeKForEven(int k, double radius, double alpha, double[] result) {
        double coeff = (k + 2.0) / 2.0;
        double first = 1.0 / coeff;
        double second = Math.pow(radius, coeff);
        double real = first * second * Math.sin(coeff * alpha);
        double image = -first * second * Math.cos(coeff * alpha);
        result[0] = real;
        result[1] = image;
        return result;
    }

    private static double[] heeKForOdd(int k, double radius, double alpha, double[] result) {
        double coeff = (k + 3.0) / 2.0;
        double first = 1.0 / coeff;
        double second = Math.pow(radius, coeff);
        double real = first * second * Math.cos(coeff * alpha);
        double image = first * second * Math.sin(coeff * alpha);

        result[0] = real;
        result[1] = image;
        return result;
    }
}
