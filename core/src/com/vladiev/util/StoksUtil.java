package com.vladiev.util;

import com.badlogic.gdx.Gdx;
import com.vladiev.Main;
import com.vladiev.model.Border;
import com.vladiev.shape.Shape;
import org.apache.log4j.Logger;
import org.la4j.Vector;
import org.la4j.linear.GaussianSolver;
import org.la4j.linear.LinearSystemSolver;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.vector.dense.BasicVector;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

/**
 * Created by mazzachuses on 06.06.15.
 */
public class StoksUtil {
    private static final Logger LOG = Logger.getLogger(StoksUtil.class);

    //todo move to Geometry util
    @Deprecated
    public static double[] createUniformMesh(double start, double end, double delta) {
        if (delta <= 0) {
            throw new IllegalArgumentException("Delta less or equal zero. Delta is " + delta);
        }
        int capacity = (int) ((end - start) / delta) + 1;
        capacity = Math.max(capacity, 1);
        double[] res = new double[capacity];
        for (int i = 0; i < res.length; ++i) {
            res[i] = delta * i;
        }
        return res;
    }


    //Warning it is likely to be bugs! todo
    public static double deltaAlpha(List<Vector> points, int i) {
        int from = (i + 1) % points.size();
        double d = points.get(from).get(0) - points.get(i).get(0);

        /*if (from == 0) {
            d += 2 * Math.PI;
        }*/
        if (Math.abs(d) > 3. / 2. * Math.PI) {
            if (d < 0) {
                d += 2 * Math.PI;
            } else {
                d -= 2 * Math.PI;
            }
        }
        return d;
    }

    public static double deltaRadius(List<Vector> points, int i) {
        int from = i + 1;
        if (from == points.size()) {
            from = 0;
        }
        return points.get(from).get(1) - points.get(i).get(1);
    }


    @Deprecated
    public static double delta(double[] arr, int i) {
        if ((i + 1) >= arr.length) {
            return 0;
        }
        return arr[i + 1] - arr[i];
    }

    /*B_k(phy)*/
    public static double harmonicPart(int k, double alpha) {
        if (k == 0) {
            return 1;
        }
        if (k % 2 == 1) {
            return Math.cos((k / 2 + 1) * alpha);
        }
        return Math.sin((k / 2) * alpha);
    }

    public static int powerOfRadius(int k) {
        if (k == 0) {
            return 0;
        }
        if (k % 2 == 1) {
            return k / 2 + 1;
        }
        return k / 2;
    }


    public static double psyK(int k, double radius, double alpha) {
        double harmonicPart = harmonicPart(k, alpha);
        double power = powerOfRadius(k);
        return harmonicPart * Math.pow(radius, power);
    }

    public static double elementOfA(int m, int k, List<Vector> polarBorder) {
        double sum = 0;
        int pRK = powerOfRadius(k);
        int pRM = powerOfRadius(m);
        for (int i = 0; i < polarBorder.size(); ++i) {
            Vector p = polarBorder.get(i);
            double angle = p.get(0);
            double radius = p.get(1);

            double bk = harmonicPart(k, angle);
            double bm = harmonicPart(m, angle);
            double g = Math.pow(radius, pRK + pRM + 2);

            double delta = deltaAlpha(polarBorder, i);
            double v = bk * bm * g * delta;
            sum += v;
        }
        sum = sum / (pRK + pRM + 2);
        return sum;
    }

    @Deprecated
    public static double elementOfA(int m, int k, Border border) {
        double[] alpha = border.getFirstCoordinates();
        double[] radius = border.getSecondCoordinates();
        BigDecimal sum = BigDecimal.ZERO;
        int pRK = powerOfRadius(k);
        int pRM = powerOfRadius(m);
        for (int i = 0; i < alpha.length; ++i) {
            double angle = alpha[i];
            double bk = harmonicPart(k, angle);
            double bm = harmonicPart(m, angle);
            double g = Math.pow(radius[i], pRK + pRM + 2);
            double delta = delta(alpha, i);
            //BigDecimal v = BigDecimal.valueOf(bk).multiply(BigDecimal.valueOf(bm)).multiply(BigDecimal.valueOf(g)).multiply(BigDecimal.valueOf(delta));
            BigDecimal v = BigDecimal.valueOf(bk * bm * g * delta);
            sum = sum.add(v);
        }
        sum = sum.divide(BigDecimal.valueOf((pRK + pRM + 2)), RoundingMode.HALF_UP);
        return sum.doubleValue();
    }

    private static double psyK(int m, Vector polarPoint) {
        double alpha = polarPoint.get(0);
        double r = polarPoint.get(1);
        return psyK(m, r, alpha);
    }

    public static double elementOfB(int m, List<Vector> deckardBorder, List<Vector> polarBorder) {
        double sum = 0;
        for (int i = 0; i < polarBorder.size(); ++i) {
            double psyK = psyK(m, polarBorder.get(i));
            Vector delta = Tool.delta(deckardBorder, i);
            double dX = delta.get(0);
            double dX2 = dX * dX;

            double dY = delta.get(1);
            double dY2 = dY * dY;

            sum += psyK * Math.sqrt(dX2 + dY2);
        }
        return sum;
    }

    @Deprecated
    public static double elementOfB(int m, Border border) {
        double sum = 0;
        double[] alpha = border.getFirstCoordinates();
        double[] radius = border.getSecondCoordinates();
        for (int i = 0; i < alpha.length; ++i) {
            double psyK = psyK(m, radius[i], alpha[i]);
            double deltaR = delta(radius, i);
            double deltaR2 = deltaR * deltaR;
            double r2 = radius[i] * radius[i];
            double deltaA = delta(alpha, i);
            double deltaA2 = deltaA * deltaA;
            sum += psyK * Math.sqrt(deltaR2 + r2 * deltaA2);
        }
        return sum;
    }

    public static double[] countPsy(double r0, double alpha0, int m) {
        double[] result = new double[m];
        for (int i = 0; i < m; ++i) {
            result[i] = psyK(i, r0, alpha0);
        }
        return result;
    }


    public static Vector countCoefficientsOfPressure(Shape shape, double tensionFactor, int numberOfHarmonicFunctions) {
        double[][] A = new double[numberOfHarmonicFunctions][numberOfHarmonicFunctions];
        double[] B = new double[numberOfHarmonicFunctions];

        for (int m = 0; m < A.length; ++m) {
            for (int k = 0; k < A[m].length; ++k) {
                A[m][k] = StoksUtil.elementOfA(m, k, shape.getPolarBorder());
            }
            B[m] = (tensionFactor / 2.) * StoksUtil.elementOfB(m, shape.getDeckardBorder(), shape.getPolarBorder());
        }
        Basic2DMatrix a = new Basic2DMatrix(A);
        LOG.debug("matrix A:\n" + a);
        LinearSystemSolver linearSystemSolver = new GaussianSolver(a);
        BasicVector b = new BasicVector(B);
        LOG.debug("vector B:\n" + b);
        Vector coefficientsOfPressure = linearSystemSolver.solve(b);
        LOG.debug("coefficients Of Pressure: \n" + coefficientsOfPressure);
        return coefficientsOfPressure;
    }


    public static List<Vector> countVelocity(Shape shape, double tensionFactor,
                                             double coefficientOfViscosity, int numberOfHarmonicFunctions) {
        List<Vector> velocities = new ArrayList<>(shape.getDeckardBorder().size());
        List<Vector> normals = shape.getNormals();
        List<Vector> polarBorder = shape.getPolarBorder();
        Vector coefficientsOfPressure = countCoefficientsOfPressure(shape, tensionFactor, numberOfHarmonicFunctions);

        for (int i = 0; i < polarBorder.size(); ++i) {
            Vector normal = normals.get(i);
            Vector res = normal.multiply(tensionFactor);
            Vector sum = new BasicVector(2);
            Vector polarPoint = polarBorder.get(i);
            for (int k = 0; k < coefficientsOfPressure.length(); ++k) {
                Vector heeK = new BasicVector(StoksUtil2.heeK(k, polarPoint.get(1), polarPoint.get(0)));
                sum = sum.add(heeK.multiply(coefficientsOfPressure.get(k)));
            }
            res = res.subtract(sum);
            res = res.multiply(1. / (2. * coefficientOfViscosity));
            velocities.add(res);
        }
        return velocities;
    }

    public static double min(double[] arr) {
        OptionalDouble min = Arrays.stream(arr).min();
        return min.getAsDouble();
    }

    public static double max(double[] arr) {
        OptionalDouble max = Arrays.stream(arr).max();
        return max.getAsDouble();
    }

}
