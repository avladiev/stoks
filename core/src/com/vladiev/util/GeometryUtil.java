package com.vladiev.util;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.Map;
import com.vladiev.model.Border;
import com.vladiev.shape.Shape;
import org.la4j.Vector;
import org.la4j.linear.GaussianSolver;
import org.la4j.linear.LinearSystemSolver;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.vector.dense.BasicVector;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;

/**
 * Created by mazzachuses on 26.02.16.
 */
public class GeometryUtil {


    public static Vector countCircleCenter(Vector a, Vector b, Vector c) {
        double x1 = b.get(0);
        double x2 = a.get(0);
        double x3 = c.get(0);
        double y1 = b.get(1);
        double y2 = a.get(1);
        double y3 = c.get(1);

        double[][] matrix = {
                {2 * (x1 - x2), 2 * (y1 - y2)},
                {2 * (x2 - x3), 2 * (y2 - y3)}
        };
        //Dx=k
        //

        Basic2DMatrix d = new Basic2DMatrix(matrix);
        //   System.out.println("matrix d;\n " + d);

        double x12 = x1 * x1;
        double x22 = x2 * x2;
        double x32 = x3 * x3;
        double y12 = y1 * y1;
        double y22 = y2 * y2;
        double y32 = y3 * y3;
        double[] vector = new double[]{
                x12 - x22 + y12 - y22,
                x22 - x32 + y22 - y32
        };

        BasicVector k = new BasicVector(vector);
        //    System.out.println("vector k:\n" + k);
        LinearSystemSolver linearSystemSolver = new GaussianSolver(d);
        Vector center = linearSystemSolver.solve(k);

        //  System.out.println("center =" + center);
        return center;
    }


    /**
     * it counts vector of normal at point by circle method
     *
     * @param a main point
     * @param b neighbor a
     * @param c neighbor a
     * @return vector of normal at point a
     */
    public static Vector countNormal(Vector a, Vector b, Vector c) {

        Vector x = a.subtract(b);
        Vector y = a.subtract(c);

        Vector z = new BasicVector(new double[]{-y.get(1), y.get(0)});

        if (z.innerProduct(x) < 0.001) {
            return z.divide(z.euclideanNorm());
        }

        Vector center = countCircleCenter(a, b, c);
        double x0 = center.get(0);
        double y0 = center.get(1);
        double x2 = a.get(0);
        double y2 = a.get(1);
        double[] normal = {
                x2 - x0,
                y2 - y0
        };
        Vector result = new BasicVector(normal);
        result = result.divide(result.euclideanNorm());
        return result;
    }

    /**
     * //https://habrahabr.ru/post/144571/
     *
     * @return external normal
     */

    public static Vector countExternalNormal(Vector normal, Vector a, Vector b) {
        Vector bc = normal;
        Vector ab = b.subtract(a);

        double z = ab.get(0) * bc.get(1) - ab.get(1) * bc.get(0);
        if (z < 0) {
            return normal;
        } else if (z > 0) {
            return normal.multiply(-1);
        }

        System.err.println("Normal: Vectors ab and bc is collinear");
        return null;
    }

    @Deprecated
    public static double countPerimeter(Border deckardBorder) {
        double res = 0;
        for (int i = 0; i < deckardBorder.length(); ++i) {
            double dX = delta(deckardBorder.getFirstCoordinates(), i);
            double dY = delta(deckardBorder.getSecondCoordinates(), i);
            res += Math.sqrt(dX * dX + dY * dY);
        }
        return res;
    }

    public static double countPerimeter(List<Vector> deckardBorder) {
        double res = 0;
        for (int i = 0; i < deckardBorder.size(); ++i) {
            Vector delta = Tool.delta(deckardBorder, i);
            double dX = delta.get(0);
            double dY = delta.get(1);
            res += Math.sqrt(dX * dX + dY * dY);
        }
        return res;
    }

    //http://algolist.manual.ru/maths/geom/polygon/area.php
    @Deprecated
    public static double countArea(Border decardBorder) {
        double[] x = decardBorder.getFirstCoordinates();
        double[] y = decardBorder.getSecondCoordinates();
        double res = 0;
        for (int i = 0; i < decardBorder.length(); ++i) {
            int nextIndex = i + 1 >= decardBorder.length() ? 0 : i + 1;
            res += (x[i] + x[nextIndex]) * (y[i] - y[nextIndex]);
        }
        return Math.abs(res) / 2.0;
    }

    //http://algolist.manual.ru/maths/geom/polygon/area.php
    public static double countArea(List<Vector> decardBorder) {
        double res = 0;
        for (int i = 0; i < decardBorder.size(); ++i) {
            Vector next = Tool.getNext(decardBorder, i);
            Vector cur = decardBorder.get(i);
            res += (cur.get(0) + next.get(0)) * (cur.get(1) - next.get(1));
        }
        return Math.abs(res) / 2.0;
    }

    private static double getIPlus1(double[] arr, int i) {
        int to = i + 1;
        if (to >= arr.length) {
            to = 0;
        }
        return to;
    }

    //copy from Stoks Util
    @Deprecated
    public static double delta(double[] arr, int from) {
        int to = from + 1;
        if ((to) >= arr.length) {
            to = 0;
        }
        return arr[to] - arr[from];
    }


    public static Vector centerOfMass(List<Vector> deckardBorder) {
        Optional<Vector> reduce = deckardBorder.stream().reduce(Vector::add);
        return reduce.get().multiply(1. / deckardBorder.size());
    }

    @Deprecated
    public static Vector centerOfMass(Border deckardBorder) {
        double[] firstCoordinates = deckardBorder.getFirstCoordinates();
        double[] secondCoordinates = deckardBorder.getSecondCoordinates();
        double xMean = Arrays.stream(firstCoordinates).average().getAsDouble();
        double yMean = Arrays.stream(secondCoordinates).average().getAsDouble();
        return new BasicVector(new double[]{xMean, yMean});
    }

    public static double angel(double x, double y) {
        double relative = Math.abs(y / x);
        double atan = Math.atan(relative);
        if (x > 0 && y >= 0) {
            return atan;
        }
        if (x < 0 && y >= 0) {
            return Math.PI - atan;
        }
        if (x < 0 && y <= 0) {
            return Math.PI + atan;
        }
        if (x > 0 && y <= 0) {
            return 2 * Math.PI - atan;
        }
        return 0;
    }

    public static Vector toPolar(Vector decardP) {
        double a = angel(decardP.get(0), decardP.get(1));
        double r = countRadius(decardP.get(0), decardP.get(1));
        return Tool.createVector(a, r);
    }


    private static double countRadius(double x, double y) {
        return Math.sqrt(x * x + y * y);
    }


    public static double[] createUniformMesh(double start, double end, double delta) {
        if (delta <= 0) {
            throw new IllegalArgumentException("Delta less or equal zero. Delta is " + delta);
        }
        int capacity = (int) ((end - start) / delta) + 1;
        capacity = Math.max(capacity, 1);
        double[] res = new double[capacity];
        for (int i = 0; i < res.length; ++i) {
            res[i] = delta * i;
        }
        return res;
    }


    static boolean isInside(List<Vector> deckardBorder, Vector p) {

        double x = p.get(0);
        double y = p.get(1);

        if (x * x + y * y <= 25) {
            return true;
        }
        return false;
      /*
        double sumAngel = 0;
        for (int i = 0; i < deckardBorder.size(); ++i) {

            int nextIndex = (i + 1) % deckardBorder.size();
            Vector pv1 = deckardBorder.get(i).subtract(p);
            Vector pv2 = deckardBorder.get(nextIndex).subtract(p);
            double det = pv1.get(0) * pv2.get(1) - pv1.get(1) * pv2.get(0);
            if (det < 0.00001 && det > -0.00001) {
                return true;
            }

            double cosVal = pv1.innerProduct(pv2) / (pv1.norm() * pv2.norm());
            double angel = Math.acos(cosVal);
            angel = angel * Math.signum(det);
            sumAngel += angel;
        }
        return sumAngel >= Math.PI;*/
    }


    static Vector[] getExternalRectangle(List<Vector> deckardBorder) {
        //точки прямоугольника в котором находиться shape;
        double a = Double.POSITIVE_INFINITY;
        double b = Double.NEGATIVE_INFINITY;

        double c = Double.POSITIVE_INFINITY;
        double d = Double.NEGATIVE_INFINITY;

        for (Vector p : deckardBorder) {
            double x = p.get(0);
            double y = p.get(1);
            if (a > x) {
                a = x;
            }
            if (b < x) {
                b = x;
            }

            if (c > y) {
                c = y;
            }
            if (d < y) {
                d = y;
            }
        }
        return new Vector[]{Tool.createVector(a, c), Tool.createVector(b, d)};
    }

}
