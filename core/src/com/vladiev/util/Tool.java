package com.vladiev.util;

import com.badlogic.gdx.math.Vector2;
import com.vladiev.model.Border;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.List;

/**
 * Created by mazzachuses on 09.04.2016.
 */
public class Tool {

    public static Vector createVector(double x, double y) {
        return new BasicVector(new double[]{x, y});
    }

    public static <T> T getPrevious(List<T> list, int index) {
        int resIndex = -1;
        if (index > 0) {
            resIndex = index - 1;
        } else {
            resIndex = list.size() - 1;
        }
        return list.get(resIndex);
    }

    public static <T> T getNext(List<T> list, int index) {
        int resIndex = -1;
        if (index < list.size() - 1) {
            resIndex = index + 1;
        } else {
            resIndex = 0;
        }
        return list.get(resIndex);
    }

    public static Vector delta(List<Vector> points, int i) {
        int from = i + 1;
        if (from == points.size()) {
            from = 0;
        }
        return points.get(from).subtract(points.get(i));
    }

    public static Vector2 getBagLogicVector(Vector vector) {
        return new Vector2(
                (float) vector.get(0),
                (float) vector.get(1)
        );
    }

}
