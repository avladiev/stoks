package com.vladiev;

import com.vladiev.model.Border;
import com.vladiev.model.DoubleArray;
import com.vladiev.shape.Ellipse;
import com.vladiev.util.StoksUtil;
import org.la4j.Vector;
import org.la4j.linear.GaussianSolver;
import org.la4j.linear.LinearSystemSolver;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;

/**
 * Created by mazzachuses on 07.06.15.
 */
public class Main {
    private static final double G = 2;
    private static final int M = 7;


    public static void main(String[] args) {
        System.out.println(new ArrayList().subList(0,0));
    }

    private static Ellipse getEllipse() {
        double a = 2.2;
        double b = a;
        double x0 = 6.3;
        double y0 = 3.4;
        double epsilon = 0.01;
        return new Ellipse(a, b, x0, y0, 0, epsilon);
    }
}
