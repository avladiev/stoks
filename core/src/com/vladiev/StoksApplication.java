package com.vladiev;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.vladiev.model.StoksModel;
import com.vladiev.model.gomoboric.GomoboricDebug;
import com.vladiev.model.gomoboric.GomoboricModelBase;
import com.vladiev.model.Model2;
import com.vladiev.model.gomoboric.GomoboricModelDeltoid;
import com.vladiev.model.gomoboric.GomoboricModelHexagonalStructure;
import com.vladiev.util.ScreenshotSaver;

import java.io.IOException;


/**
 * Created by mazzachuses on 14.02.16.
 */
public class StoksApplication extends ApplicationAdapter {
    private static final float SCALE = 1f;
    OrthographicCamera camera;
    SpriteBatch batch;
    ShapeRenderer renderer;
    float width;
    float height;

    Model2 model;

    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();
        Gdx.app.log("init", "width=" + width + ", height=" + height);
        ;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, width / SCALE, height / SCALE);

        batch = new SpriteBatch();
        batch.setProjectionMatrix(camera.combined);
        renderer = new ShapeRenderer();
        model = createModel();
        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.SPACE) {
                    model.setRunning(!model.isRunning());
                }
                if (keycode == Input.Keys.Z) {
                    try {
                        ScreenshotSaver.saveScreenshot("xxxx");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return super.keyDown(keycode);
            }
        });

    }


    private void handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
            camera.zoom += camera.zoom * 0.02f;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            camera.zoom -= camera.zoom * 0.02f;
            if (camera.zoom < 0) {
                camera.zoom = 0;
            }
        }
        float x = 0;
        float y = 0;
        float velocity = 4 * camera.zoom;
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            x = -velocity;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            x = velocity;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            y = -velocity;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            y = velocity;
        }

        camera.translate(x, y);

    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glLineWidth(2);

        handleInput();
        camera.update();
        renderer.setProjectionMatrix(camera.combined);

        model.render(renderer);
    }

    private static Model2 createModel() {
        return new StoksModel();
    }
}
