package com.vladiev.util;

import com.vladiev.model.Border;
import com.vladiev.shape.Ellipse;
import org.junit.Test;

/**
 * Created by mazzachuses on 07.06.15.
 */
@Deprecated
public class ElementOfBTest {


    @Test
    public void testB0() {
        int r = 2;
        double epsilon = 0.01;
        double accepted = 2 * r * Math.PI;
        Border polarBorder = new Ellipse(r, r, 5, 11, 0, epsilon).getPolarBorder();
        double actual = StoksUtil.elementOfB(0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testB0Ellipse() {
        double a = 3.1;
        double b = 2.0;
        double epsilon = 0.01;
        double accepted = 4 * (a * b * Math.PI + Math.pow(a - b, 2)) / (a + b);
        Border polarBorder = new Ellipse(a, b, 5, 11, .6, epsilon).getPolarBorder();
        double actual = StoksUtil.elementOfB(0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }


    @Test
    public void testB1() {
        double r = 1.4;
        double x0 = 6.3;
        double y0 = 3.4;
        double epsilon = 0.01;
        double accepted = 2 * Math.PI * r * x0;
        Border polarBorder = new Ellipse(r, r, x0, y0, 0, epsilon).getPolarBorder();
        double actual = StoksUtil.elementOfB(1, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }


    @Test
    public void testB1Ellipse() {
        double a = 3.1;
        double b = 2.0;
        double x0 = 6.3;
        double y0 = 3.4;
        double epsilon = 0.001;
        double accepted = x0 * 4 * (a * b * Math.PI + Math.pow(a - b, 2)) / (a + b);
        Border polarBorder = new Ellipse(a, b, x0, y0, .6, epsilon).getPolarBorder();
        double actual = StoksUtil.elementOfB(1, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 1);
    }

    @Test
    public void testB2() {
        double r = 1.9;
        double x0 = 6.3;
        double y0 = 3.4;
        double epsilon = 0.01;
        double accepted = 2 * Math.PI * r * y0;
        Border polarBorder = new Ellipse(r, r, x0, y0, 0, epsilon).getPolarBorder();
        double actual = StoksUtil.elementOfB(2, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testB2Ellipse() {
        double a = 3.1;
        double b = 2.5;
        double x0 = 6.3;
        double y0 = 3.4;
        double epsilon = 0.001;
        double accepted = y0 * 4 * (a * b * Math.PI + Math.pow(a - b, 2)) / (a + b);
        Border polarBorder = new Ellipse(a, b, x0, y0, .8, epsilon).getPolarBorder();
        double actual = StoksUtil.elementOfB(2, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }
}
