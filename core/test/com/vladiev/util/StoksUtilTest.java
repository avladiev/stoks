package com.vladiev.util;

import com.vladiev.model.Border;
import com.vladiev.shape.Ellipse;
import com.vladiev.shape.Shape;
import com.vladiev.shape.ShapeFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.la4j.Vector;

import java.util.List;

/**
 * Created by mazzachuses on 09.04.2016.
 */
public class StoksUtilTest {

    // element of matrix A tests
    @Test
    public void testCircle1() {
        int r = 2;
        double epsilon = 0.01;
        double[][] accepted = new double[][]{{12.5664, 0.0000226575, 0.000743693, 0.000135902, 0.00222831,
                0.000488987, 0.00533688, 0.00144777}, {0.0000226575, 12.5664,
                0.00111416, 0.000271683, 0.00356087, 0.000905089, 0.00888373,
                0.00263568}, {0.000743693, 0.00111416, 12.5663,
                0.00177601, -0.000217304, 0.00294157, -0.000542684,
                0.00499846}, {0.000135902, 0.000271683, 0.00177601, 33.511,
                0.00591265, 0.00201481, 0.0151724, 0.00541946}, {0.00222831,
                0.00356087, -0.000217304, 0.00591265, 33.5096,
                0.0100728, -0.00185944, 0.0174726}, {0.000488987, 0.000905089,
                0.00294157, 0.00201481, 0.0100728, 100.536, 0.0263858,
                0.0120237}, {0.00533688, 0.00888373, -0.000542684,
                0.0151724, -0.00185944, 0.0263858, 100.526, 0.0464959}, {0.00144777,
                0.00263568, 0.00499846, 0.00541946, 0.0174726, 0.0120237,
                0.0464959, 321.727}};
        Shape circle = ShapeFactory.createCircle(r, 0, 0, epsilon);
        List<Vector> polarBorder = circle.getPolarBorder();
        for (int i = 0; i < accepted.length; ++i) {
            for (int j = 0; j < accepted.length; ++j) {
                double actual = StoksUtil.elementOfA(i, j, polarBorder);
                DoubleAssert.assertEquals(accepted[i][j], actual, 0.1);
            }
        }
    }

    @Test
    public void testCircleArea1() {
        int r = 2;
        double epsilon = 0.1;
        double accepted = r * r * Math.PI;
        Shape circle = ShapeFactory.createCircle(r, 5, 11, epsilon);
        List<Vector> polarBorder = circle.getPolarBorder();
        double actual = StoksUtil.elementOfA(0, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testCircleArea2() {
        int r = 3;
        double epsilon = 0.01;
        double accepted = r * r * Math.PI;
        Shape circle = ShapeFactory.createEllipse(r, r, 5, 11, Math.PI / 3, epsilon);
        List<Vector> polarBorder = circle.getPolarBorder();
        double actual = StoksUtil.elementOfA(0, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testCircleArea3() {
        double r = 1234.0;
        double epsilon = 0.0001;
        double accepted = r * r * Math.PI;
        double[][] sings = new double[][]{{1, 1}, {-1, 1}, {-1, -1}, {1, -1}};

        for (double[] sing : sings) {
            Shape circle = ShapeFactory.createEllipse(r, r, sing[0] * 98763, sing[1] * 97543, Math.PI / 3, epsilon);
            List<Vector> polarBorder = circle.getPolarBorder();
            double actual = StoksUtil.elementOfA(0, 0, polarBorder);
            DoubleAssert.assertEquals(accepted, actual, 10000);
        }
    }

    @Test
    public void testCircleA01() {
        double r = 2.0;
        double epsilon = 0.001;
        int x0 = 6;
        double accepted = r * r * Math.PI * x0;
        int y0 = 7;

        Shape circle = ShapeFactory.createEllipse(r, r, x0, y0, 0, epsilon);
        List<Vector> polarBorder = circle.getPolarBorder();
        double actual = StoksUtil.elementOfA(1, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
        double actual2 = StoksUtil.elementOfA(0, 1, polarBorder);
        DoubleAssert.assertEquals(actual, actual2, 0.1);
    }

    @Test
    public void testCircleA01Rotate() {
        double r = 2.0;
        double epsilon = 0.001;
        int x0 = 6;
        double accepted = r * r * Math.PI * x0;
        Shape circle = ShapeFactory.createEllipse(r, r, x0, 7, Math.PI / 12, epsilon);
        List<Vector> polarBorder = circle.getPolarBorder();
        double actual = StoksUtil.elementOfA(1, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
        double actual2 = StoksUtil.elementOfA(0, 1, polarBorder);
        DoubleAssert.assertEquals(actual, actual2, 0.1);
    }

    @Test
    public void testCircleA11() {
        double r = 5.1;
        double epsilon = 0.0001;
        double x0 = 6.2;
        double r2 = r * r;
        double accepted = r2 * Math.PI * x0 * x0 + (r2 * r2) * Math.PI / 4;
        Shape circle = ShapeFactory.createEllipse(r, r, x0, 9.9, Math.PI / 6, epsilon);
        List<Vector> polarBorder = circle.getPolarBorder();
        double actual = StoksUtil.elementOfA(1, 1, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testCircleA20() {
        double r = 5.1;
        double epsilon = 0.0001;
        double x0 = 6.2;
        double y0 = 9.9;
        double r2 = r * r;
        double accepted = Math.PI * r2 * y0;

        Shape circle = ShapeFactory.createEllipse(r, r, x0, y0, Math.PI / 4, epsilon);
        List<Vector> polarBorder = circle.getPolarBorder();
        double actual = StoksUtil.elementOfA(2, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
        double actual2 = StoksUtil.elementOfA(2, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual2, 0.1);
    }

    @Test
    public void testCircleA30() {
        double r = 1;
        double epsilon = 0.0001;
        double x0 = 10;
        double y0 = 10;
        double r2 = r * r;
        double accepted = 0.01; //todo
        Shape circle = ShapeFactory.createEllipse(r, r, x0, y0, Math.PI / 4, epsilon);
        List<Vector> polarBorder = circle.getPolarBorder();
        double actual = StoksUtil.elementOfA(0, 3, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testEllipse1() {
        int a = 4;
        int b = 2;
        double epsilon = 0.001;
        double[][] accepted = new double[][]
                {{25.1327, 0, 0, 75.3983, 0.0335127,
                        0, 0.0000115944, 452.389}, {0, 100.531,
                        0.0167564, 0, 0, 603.186, 0.197377,
                        0}, {0, 0.0167564, 25.1327,
                        0, 0, -0.00371044, 150.797,
                        0.000044169}, {75.3983, 0, 0, 720.472,
                        0.0968334, 0, 0.000132507, 5805.66}, {0.0335127,
                        0, 0, 0.0968334, 268.083,
                        0.000088338, 0, -0.0406477}, {0,
                        603.186, -0.00371044, 0, 0.000088338, 6534.51, 0.713489,
                        0}, {0.0000115944, 0.197377, 150.797,
                        0.000132507, 0, 0.713489, 3141.59,
                        0.00164898}, {452.389, 0, 0.000044169,
                        5805.66, -0.0406477, 0, 0.00164898, 67747.8}};
        Shape ellipse = ShapeFactory.createEllipse(a, b, 0, 0, 0, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        for (int i = 0; i < accepted.length; ++i) {
            for (int j = 0; j < accepted.length; ++j) {
                double actual = StoksUtil.elementOfA(i, j, polarBorder);
                DoubleAssert.assertEquals(accepted[i][j], actual, 0.1);
            }
        }
    }

    //element of B tests
    @Test
    public void testBStress1() {
        int r = 2;
        double epsilon = 1.;
        double[] accepted = new double[]{12.0707, 0.179535, 0.360202, 1.18283, 1.23097, 5.17901, 2.55412,
                19.0233};
        Shape ellipse = ShapeFactory.createEllipse(r, r, 0, 0, 0, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        List<Vector> deckardBorder = ellipse.getDeckardBorder();
        for (int i = 0; i < accepted.length; ++i) {
            double actual = StoksUtil.elementOfB(i, deckardBorder, polarBorder);
            DoubleAssert.assertEquals(accepted[i], actual, 0.1);
        }

    }

    //element of B tests
    @Test
    public void testBStress2() {
        int r = 2;
        double epsilon = 0.005;
        double[] accepted = new double[]{12.5664, 0, 0.0000115606, 0, 0.000046242,
                0, 0.000138723, 0};

        Shape ellipse = ShapeFactory.createEllipse(r, r, 0, 0, 0, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        List<Vector> deckardBorder = ellipse.getDeckardBorder();
        for (int i = 0; i < accepted.length; ++i) {
            double actual = StoksUtil.elementOfB(i, deckardBorder, polarBorder);
            DoubleAssert.assertEquals(accepted[i], actual, 0.1);
        }
    }

    //element of B tests------------------------------------------------------------------------------------------------
    @Test
    public void testBStress3() {
        int a = 15;
        int b = 21;
        double epsilon = 0.005;
        double[] accepted = new double[]{113.884,
                0, 0.00127456, -9172., -14.754, 0.00901117, 0.860296,
                1.27601436e6};

        Shape ellipse = ShapeFactory.createEllipse(a, b, 0, 0, 0, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        List<Vector> deckardBorder = ellipse.getDeckardBorder();
        for (int i = 0; i < accepted.length; ++i) {
            double actual = StoksUtil.elementOfB(i, deckardBorder, polarBorder);
            DoubleAssert.assertEquals(accepted[i], actual, 0.1);
        }
    }

    @Test
    public void testB0() {
        int r = 2;
        double epsilon = 0.01;
        double accepted = 2 * r * Math.PI;

        Shape ellipse = ShapeFactory.createEllipse(r, r, 5, 11, 0, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        List<Vector> deckardBorder = ellipse.getDeckardBorder();

        double actual = StoksUtil.elementOfB(0, deckardBorder, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testB0Ellipse() {
        double a = 3.1;
        double b = 2.0;
        double epsilon = 0.01;
        double accepted = 4 * (a * b * Math.PI + Math.pow(a - b, 2)) / (a + b);
        Shape ellipse = ShapeFactory.createEllipse(a, b, 5, 11, 0.6, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        List<Vector> deckardBorder = ellipse.getDeckardBorder();
        double actual = StoksUtil.elementOfB(0, deckardBorder, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testB1() {
        double r = 1.4;
        double x0 = 6.3;
        double y0 = 3.4;
        double epsilon = 0.01;
        double accepted = 2 * Math.PI * r * x0;

        Shape ellipse = ShapeFactory.createEllipse(r, r, x0, y0, 0, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        List<Vector> deckardBorder = ellipse.getDeckardBorder();
        double actual = StoksUtil.elementOfB(1, deckardBorder, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testB1Ellipse() {
        double a = 3.1;
        double b = 2.0;
        double x0 = 6.3;
        double y0 = 3.4;
        double epsilon = 0.001;
        double accepted = x0 * 4 * (a * b * Math.PI + Math.pow(a - b, 2)) / (a + b);

        Shape ellipse = ShapeFactory.createEllipse(a, b, x0, y0, 0.6, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        List<Vector> deckardBorder = ellipse.getDeckardBorder();

        double actual = StoksUtil.elementOfB(1, deckardBorder, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 1);
    }

    @Test
    public void testB2() {
        double r = 1.9;
        double x0 = 6.3;
        double y0 = 3.4;
        double epsilon = 0.01;
        double accepted = 2 * Math.PI * r * y0;


        Shape ellipse = ShapeFactory.createEllipse(r, r, x0, y0, 0, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        List<Vector> deckardBorder = ellipse.getDeckardBorder();
        double actual = StoksUtil.elementOfB(2, deckardBorder, polarBorder);

        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testB2Ellipse() {
        double a = 3.1;
        double b = 2.5;
        double x0 = 6.3;
        double y0 = 3.4;
        double epsilon = 0.001;
        double accepted = y0 * 4 * (a * b * Math.PI + Math.pow(a - b, 2)) / (a + b);

        Shape ellipse = ShapeFactory.createEllipse(a, b, x0, y0, .8, epsilon);
        List<Vector> polarBorder = ellipse.getPolarBorder();
        List<Vector> deckardBorder = ellipse.getDeckardBorder();

        double actual = StoksUtil.elementOfB(2, deckardBorder, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    //coefficient of pressure test -------------------------------------------------------------------------------------

    @Test
    public void testCP() {
        double a = 4.5;
        double b = 2.;
        double epsilon = 0.05;
        double tensionFactor = 1.;
        int numberOfFunction = 8;

        double[] accepted = new double[]{0.301955,
                1.33181e-6, -6.47604e-6, 0.00708174, 0.0000987034,
                -3.9441e-7, -1.27423e-6, 0.00132033};
        Shape ellipse = ShapeFactory.createEllipse(a, b, 0, 0, 0, epsilon);
        Vector doubles = StoksUtil.countCoefficientsOfPressure(ellipse, tensionFactor, numberOfFunction);
        Assert.assertEquals(accepted.length, doubles.length());
        for (int i = 0; i < accepted.length; ++i) {
            DoubleAssert.assertEquals(accepted[i], doubles.get(i), 0.001);
        }
    }

    //tests of velocities ---------------------------------------------------------------------------------------------
    @Test
    @Ignore
    public void testVelocities() {
        double a = 4.5;
        double b = 2.;
        double epsilon = 1;
        double tensionFactor = 1.;
        int numberOfFunction = 8;
        double mu = 1;
        Shape ellipse = ShapeFactory.createEllipse(a, b, 0, 0, 0, epsilon);
        List<Vector> vectors = StoksUtil.countVelocity(ellipse, tensionFactor, mu, numberOfFunction);
        System.out.println(vectors);
    }

}
