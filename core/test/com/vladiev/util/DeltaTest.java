package com.vladiev.util;

import org.junit.Test;

/**
 * Created by mazzachuses on 07.06.15.
 */
public class DeltaTest {
    @Test
    public void testDelta1(){
        double delta = StoksUtil.delta(new double[0], 10);
        DoubleAssert.assertEquals(delta,0,0.01);
    }

    @Test
    public void testDelta2(){
        double delta = StoksUtil.delta(new double[]{1}, 0);
        DoubleAssert.assertEquals(delta,0,0.01);
    }

    @Test
    public void testDelta3(){
        double delta = StoksUtil.delta(new double[]{1, 2}, 0);
        DoubleAssert.assertEquals(delta,1,0.01);
    }

    @Test
    public void testDelta4(){
        double delta = StoksUtil.delta(new double[]{1, 2}, 1);
        DoubleAssert.assertEquals(delta,0,0.01);
    }
    @Test
    public void testDelta5(){
        double delta = StoksUtil.delta(new double[]{1, 2, -2.1}, 1);
        DoubleAssert.assertEquals(delta,-4.1,0.01);
    }
}
