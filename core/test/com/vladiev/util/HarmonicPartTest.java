package com.vladiev.util;

import org.junit.Test;

/**
 * Created by mazzachuses on 07.06.15.
 */
public class HarmonicPartTest {
    public static final double EPSILON = 0.001;
    @Test
    public void testHarmonicPart1(){
        double v = StoksUtil.harmonicPart(0, 0.1);
        DoubleAssert.assertEquals(1,v,EPSILON);
    }

    @Test
    public void testHarmonicPart2(){
        double alpha = 0.1;
        double actual = StoksUtil.harmonicPart(1, alpha);
        double excepted = Math.cos(alpha);
        DoubleAssert.assertEquals(excepted,actual,EPSILON);
    }

    @Test
    public void testHarmonicPart3(){
        double angel = 0.1;
        double actual = StoksUtil.harmonicPart(2, angel);
        double excepted = Math.sin(angel);
        DoubleAssert.assertEquals(excepted,actual,EPSILON);
    }

    @Test
    public void testHarmonicPart4(){
        double actual = StoksUtil.harmonicPart(4, Math.PI);
        double excepted = 0.0;
        DoubleAssert.assertEquals(excepted,actual,EPSILON);
    }

    @Test
    public void testHarmonicPart5(){
        double actual = StoksUtil.harmonicPart(3, Math.PI);
        double excepted = 1.0;
        DoubleAssert.assertEquals(excepted,actual,EPSILON);
    }


    @Test
    public void testHarmonicPart6(){
        double actual = StoksUtil.harmonicPart(3, Math.PI / 8);
        double excepted = Math.sqrt(2)/2;
        DoubleAssert.assertEquals(excepted,actual,EPSILON);
    }


}
