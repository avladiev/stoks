package com.vladiev.util;

import org.junit.Test;

/**
 * Created by mazzachuses on 07.06.15.
 */
public class PsyKTest {
    private static final double EPSILON = 0.00001;

    @Test
    public void test1() {
        double actual = StoksUtil.psyK(0, 2.1, 0.1);
        double excepted = 1;
        DoubleAssert.assertEquals(excepted, actual, EPSILON);
    }
    @Test
    public void test2() {
        double actual = StoksUtil.psyK(1, 2.1, 0.1);
        double excepted = 2.08950;
        DoubleAssert.assertEquals(excepted, actual, EPSILON);
    }

    @Test
    public void test3() {
        double actual = StoksUtil.psyK(2, 2.1, 0.1);
        double excepted = 0.209650;
        DoubleAssert.assertEquals(excepted, actual, EPSILON);
    }

    @Test
    public void test4() {
        double actual = StoksUtil.psyK(9, 2.1, Math.PI);
        double excepted = -40.84101;
        DoubleAssert.assertEquals(excepted, actual, EPSILON);
    }
}
