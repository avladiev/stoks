package com.vladiev.util;

import com.vladiev.model.Border;
import com.vladiev.shape.Circle;
import com.vladiev.shape.Ellipse;
import org.junit.Test;

/**
 * Created by mazzachuses on 29.01.16.
 */
@Deprecated
public class ElementOfATest {
    //параметры внешней окружности
    private static final double R0 = 5; //координыты
    private static final double PHI0 = Math.PI / 4;
    private static final double R = 4; // радиус

    private static final double EPSILON = 0.01;
    private static final Border BORDER = new Circle(R0, PHI0, R).createBorder(EPSILON);

    @Test
    public void testCircleArea1() {
        int r = 2;
        double epsilon = 1;
        double accepted = r * r * Math.PI;
        Border polarBorder = new Ellipse(r, r, 5, 11, 0, epsilon).getPolarBorder();
        double actual = StoksUtil.elementOfA(0, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }


    @Test
    public void testCircleArea2() {
        int r = 3;
        double epsilon = 0.01;
        double accepted = r * r * Math.PI;
        Ellipse ellipse = new Ellipse(r, r, 11, 6, Math.PI / 3, epsilon);
        Border polarBorder = ellipse.getPolarBorder();
        System.out.println(polarBorder);
        double actual = StoksUtil.elementOfA(0, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }


    @Test
    public void testCircleArea3() {
        double r = 1234.0;
        double epsilon = 0.0001;
        double accepted = r * r * Math.PI;
        Ellipse ellipse = new Ellipse(r, r, 98763, 97543, Math.PI * 10.0 / 3, epsilon);
        Border polarBorder = ellipse.getPolarBorder();
        double actual = StoksUtil.elementOfA(0, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 10000);
    }


    @Test
    public void testCircleA01() {
        double r = 2.0;
        double epsilon = 0.01;
        int x0 = 6;
        double accepted = r * r * Math.PI * x0;
        int y0 = 7;
        Ellipse ellipse = new Ellipse(r, r, x0, y0, 0, epsilon);
        Border polarBorder = ellipse.getPolarBorder();
        double actual = StoksUtil.elementOfA(1, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.01);
        double actual2 = StoksUtil.elementOfA(0, 1, polarBorder);
        DoubleAssert.assertEquals(actual, actual2, 0.01);
    }

    @Test
    public void testCircleA01Rotate() {
        double r = 2.0;
        double epsilon = 0.01;
        int x0 = 6;
        double accepted = r * r * Math.PI * x0;
        Ellipse ellipse = new Ellipse(r, r, x0, 7, Math.PI / 12, epsilon);
        Border polarBorder = ellipse.getPolarBorder();
        double actual = StoksUtil.elementOfA(1, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
        double actual2 = StoksUtil.elementOfA(0, 1, polarBorder);
        DoubleAssert.assertEquals(actual, actual2, 0.1);
    }

    @Test
    public void testCircleA11() {
        double r = 5.1;
        double epsilon = 0.0001;
        double x0 = 6.2;
        double r2 = r * r;
        double accepted = r2 * Math.PI * x0 * x0 + (r2 * r2) * Math.PI / 4;
        Ellipse ellipse = new Ellipse(r, r, x0, 9.9, Math.PI / 6, epsilon);
        Border polarBorder = ellipse.getPolarBorder();
        double actual = StoksUtil.elementOfA(1, 1, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }


    @Test
    public void testCircleA20() {
        double r = 5.1;
        double epsilon = 0.0001;
        double x0 = 6.2;
        double y0 = 9.9;
        double r2 = r * r;
        double accepted = Math.PI * r2 * y0;
        Ellipse ellipse = new Ellipse(r, r, x0, y0, Math.PI / 4, epsilon);
        Border polarBorder = ellipse.getPolarBorder();
        double actual = StoksUtil.elementOfA(2, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
        double actual2 = StoksUtil.elementOfA(2, 0, polarBorder);
        DoubleAssert.assertEquals(accepted, actual2, 0.1);
    }


    @Test
    public void testCircleA30() {
        double r = 1;
        double epsilon = 0.0001;
        double x0 = 10;
        double y0 = 10;
        double r2 = r * r;
        double accepted = 0.01; //todo
        Ellipse ellipse = new Ellipse(r, r, x0, y0, 0, epsilon);
        Border polarBorder = ellipse.getPolarBorder();
        double actual = StoksUtil.elementOfA(0, 3, polarBorder);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }




}
