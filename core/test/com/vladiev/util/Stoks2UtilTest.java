package com.vladiev.util;

import org.junit.Test;

import java.util.Arrays;

/**
 * Created by mazzachuses on 12.03.16.
 */
public class Stoks2UtilTest {

    @Test
    public void testHeeK0() throws Exception {
        double[] actual = StoksUtil2.heeK(0, 2.0, Math.PI / 4);
        DoubleAssert.assertEquals(new double[]{1.4, 1.4}, actual, 0.1);
    }

    @Test
    public void testHeeK1() throws Exception {
        double[] actual = StoksUtil2.heeK(1, 2.0, Math.PI / 4);
        DoubleAssert.assertEquals(new double[]{0, 2}, actual, 0.1);
    }

    @Test
    public void testHeeK2() throws Exception {
        double[] actual = StoksUtil2.heeK(2, 2.0, Math.PI / 4);
        DoubleAssert.assertEquals(new double[]{-2, 0}, actual, 0.1);
    }
}