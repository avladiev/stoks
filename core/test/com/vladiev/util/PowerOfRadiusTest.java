package com.vladiev.util;

import org.junit.Test;

/**
 * Created by mazzachuses on 07.06.15.
 */
public class PowerOfRadiusTest {
    @Test
    public void testPowerOfRadius1(){
        double actual = StoksUtil.powerOfRadius(0);
        double excepted= 0;
        DoubleAssert.assertEquals(excepted,actual,0.1);
    }

    @Test
    public void testPowerOfRadius2(){
        double actual = StoksUtil.powerOfRadius(3);
        double excepted= 2;
        DoubleAssert.assertEquals(excepted,actual,0.1);
    }

    @Test
    public void testPowerOfRadius3(){
        double actual = StoksUtil.powerOfRadius(4);
        double excepted= 2;
        DoubleAssert.assertEquals(excepted,actual,0.1);
    }

    @Test
    public void testPowerOfRadius4(){
        double actual = StoksUtil.powerOfRadius(1);
        double excepted= 1;
        DoubleAssert.assertEquals(excepted,actual,0.1);
    }
}
