package com.vladiev.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mazzachuses on 07.06.15.
 */
public class CreateUniformMeshTest {

    @Test(expected = IllegalArgumentException.class)
    public void testCreateUniformMesh1() {
        StoksUtil.createUniformMesh(0, 1, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateUniformMesh2() {
        StoksUtil.createUniformMesh(0, 1, -1);
    }

    @Test
    public void testCreateUniformMesh3() {
        double[] uniformMesh = StoksUtil.createUniformMesh(0, 1, 2);
        Assert.assertEquals(uniformMesh.length, 1);
    }
}
