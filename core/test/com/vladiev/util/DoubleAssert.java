package com.vladiev.util;

import org.junit.Assert;

/**
 * Created by mazzachuses on 07.06.15.
 */
public class DoubleAssert {

    public static void assertEquals(double expected, double actual, double epsilon) {
        if (Math.abs(expected - actual) > epsilon) {
            Assert.assertTrue("\nExpected :" + expected + "\n" + "Actual   :" + actual + "\n", false);
        }
    }

    public static void assertEquals(double[] expected, double[] actual, double epsilon) {
        Assert.assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; ++i) {
            assertEquals(expected[i], actual[i], epsilon);
        }
    }

}
