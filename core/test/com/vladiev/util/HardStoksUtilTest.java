package com.vladiev.util;


import com.vladiev.shape.Shape;
import com.vladiev.shape.ShapeFactory;
import org.junit.Assert;
import org.junit.Test;
import org.la4j.Vector;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mazzachuses on 24.04.16.
 */
public class HardStoksUtilTest {

    @Test
    public void testCountDoubleIntegral() throws Exception {
        int r = 2;
        double epsilon = 0.01;
        double[][] accepted = new double[][]{
                {12.5664, 0.0000226575, 0.000743693, 0.000135902, 0.00222831, 0.000488987, 0.00533688, 0.00144777},
                {0.0000226575, 12.5664, 0.00111416, 0.000271683, 0.00356087, 0.000905089, 0.00888373, 0.00263568},
                {0.000743693, 0.00111416, 12.5663, 0.00177601, -0.000217304, 0.00294157, -0.000542684, 0.00499846},
                {0.000135902, 0.000271683, 0.00177601, 33.511, 0.00591265, 0.00201481, 0.0151724, 0.00541946},
                {0.00222831, 0.00356087, -0.000217304, 0.00591265, 33.5096, 0.0100728, -0.00185944, 0.0174726},
                {0.000488987, 0.000905089, 0.00294157, 0.00201481, 0.0100728, 100.536, 0.0263858, 0.0120237},
                {0.00533688, 0.00888373, -0.000542684,
                        0.0151724, -0.00185944, 0.0263858, 100.526, 0.0464959}, {0.00144777,
                0.00263568, 0.00499846, 0.00541946, 0.0174726, 0.0120237,
                0.0464959, 321.727}};
        Shape circle = ShapeFactory.createCircle(r, 0, 0, epsilon);
/*
        double actual = HardStoksUtil.elementOfA(0, 0, circle, null);
        System.out.println("actual: "+actual +", accepted:"+accepted[0][0]);


        actual = HardStoksUtil.elementOfA(1, 1, circle, null);
        System.out.println("actual: "+actual +", accepted:"+accepted[1][1]);

        actual = HardStoksUtil.elementOfA(2, 2, circle, null);
        System.out.println("actual: "+actual +", accepted:"+accepted[2][2]);

        actual = HardStoksUtil.elementOfA(0, 1, circle, null);
        System.out.println("actual: "+actual +", accepted:"+accepted[0][1]);*/


        for (int i = 0; i < accepted.length; ++i) {
            for (int j = 0; j < accepted.length; ++j) {
                double actual = HardStoksUtil.elementOfA(i, j, circle, null);

                System.out.println(i + "," + j + ":actual:" + actual + ", accepted:" + accepted[i][j]);
                //  DoubleAssert.assertEquals(accepted[i][j], actual, 0.1);
            }
        }
    }


    @Test
    public void testCircleArea2() {
        int r = 3;
        double epsilon = 0.01;
        double accepted = r * r * Math.PI;
        Shape circle = ShapeFactory.createEllipse(r, r, 5, 11, Math.PI / 3, epsilon);
        List<Vector> polarBorder = circle.getPolarBorder();
        double actual = HardStoksUtil.elementOfA(0, 0, circle, null);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testCircleA01() {
        double r = 2.0;
        double epsilon = 0.001;
        int x0 = 6;
        double accepted = r * r * Math.PI * x0;
        int y0 = 7;

        Shape circle = ShapeFactory.createEllipse(r, r, x0, y0, 0, epsilon);
        double actual = HardStoksUtil.elementOfA(1, 0, circle, null);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
        double actual2 = HardStoksUtil.elementOfA(0, 1, circle, null);
        DoubleAssert.assertEquals(actual, actual2, 0.1);
    }

    @Test
    public void testCircleA11() {
        double r = 5.1;
        double epsilon = 0.0001;
        double x0 = 6.2;
        double r2 = r * r;
        double accepted = r2 * Math.PI * x0 * x0 + (r2 * r2) * Math.PI / 4;
        Shape circle = ShapeFactory.createEllipse(r, r, x0, 9.9, Math.PI / 6, epsilon);
        double actual = HardStoksUtil.elementOfA(1, 1, circle, null);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }

    @Test
    public void testCircleA20() {
        double r = 5.1;
        double epsilon = 0.0001;
        double x0 = 6.2;
        double y0 = 9.9;
        double r2 = r * r;
        double accepted = Math.PI * r2 * y0;

        Shape circle = ShapeFactory.createEllipse(r, r, x0, y0, Math.PI / 4, epsilon);
        double actual = HardStoksUtil.elementOfA(2, 0, circle, null);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
        double actual2 = HardStoksUtil.elementOfA(2, 0, circle, null);
        DoubleAssert.assertEquals(accepted, actual2, 0.1);
    }

    @Test
    public void testCircleA30() {
        double r = 1;
        double epsilon = 0.0001;
        double x0 = 10;
        double y0 = 10;
        double r2 = r * r;
        double accepted = 0.01; //todo
        Shape circle = ShapeFactory.createEllipse(r, r, x0, y0, Math.PI / 4, epsilon);
        double actual = HardStoksUtil.elementOfA(0, 3, circle, null);
        DoubleAssert.assertEquals(accepted, actual, 0.1);
    }


    //coefficient of pressure test -------------------------------------------------------------------------------------
    @Test
    public void testCP() {
        double a = 4.5;
        double b = 2.;
        double epsilon = 0.05;
        double tensionFactor = 1.;

        double[] accepted = new double[]{0.301955,
                1.33181e-6, -6.47604e-6, 0.00708174, 0.0000987034,
                -3.9441e-7, -1.27423e-6, 0.00132033};
        Shape ellipse = ShapeFactory.createEllipse(a, b, 0, 0, 0, epsilon);
        Vector countCoefficientsOfPressure = HardStoksUtil.countCoefficientsOfPressure(ellipse, new ArrayList<Shape>(), tensionFactor);
        System.out.println(countCoefficientsOfPressure);
        Assert.assertEquals(accepted.length, countCoefficientsOfPressure.length());
        for (int i = 0; i < accepted.length; ++i) {
            DoubleAssert.assertEquals(accepted[i], countCoefficientsOfPressure.get(i), 0.001);
        }
    }


}