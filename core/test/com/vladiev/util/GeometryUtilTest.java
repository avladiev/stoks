package com.vladiev.util;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import com.vladiev.model.Border;
import com.vladiev.shape.Ellipse;
import com.vladiev.shape.Shape;
import com.vladiev.shape.ShapeFactory;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Assert;
import org.junit.Test;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by mazzachuses on 26.02.16.
 */
public class GeometryUtilTest {

    @Test
    public void testCountNormal() {
        BasicVector a = new BasicVector(new double[]{
                0, 3
        });

        BasicVector b = new BasicVector(new double[]{
                -3, 0
        });
        BasicVector c = new BasicVector(new double[]{
                3, 0
        });

        Vector actual = GeometryUtil.countNormal(a, b, c);
        double[] expected = {0, 1};
        double[] actualArray = new double[actual.length()];
        for (int i = 0; i < actualArray.length; ++i) {
            actualArray[i] = actual.get(i);
        }
        DoubleAssert.assertEquals(expected, actualArray, 0.0);

        actual = GeometryUtil.countNormal(a, c, b);
        DoubleAssert.assertEquals(expected, actualArray, 0.0);

    }

    @Test
    public void testCountNormal2() {
        double r = 1000;
        BasicVector a = new BasicVector(new double[]{
                0.707106781 * r, 0.707106781 * r
        });

        BasicVector b = new BasicVector(new double[]{
                r, 0
        });
        BasicVector c = new BasicVector(new double[]{
                0, r
        });

        Vector actual = GeometryUtil.countNormal(a, b, c);
        double[] expected = {0.707, 0.707,};
        double[] actualArray = new double[actual.length()];
        for (int i = 0; i < actualArray.length; ++i) {
            actualArray[i] = actual.get(i);
        }
        DoubleAssert.assertEquals(expected, actualArray, 0.1);
    }

    @Test
    public void testCountExternalNormal2() {
        Vector normal = new BasicVector(new double[]{1, 0});
        Vector a = new BasicVector(new double[]{1, 0});
        Vector b = new BasicVector(new double[]{1, 1});
        Vector actual = GeometryUtil.countExternalNormal(normal, a, b);
        Assert.assertEquals(normal.length(), actual.length());
        DoubleAssert.assertEquals(normal.get(0), actual.get(0), 0.1);
        DoubleAssert.assertEquals(normal.get(1), actual.get(1), 0.1);

    }

    @Test
    public void testCountExternalNormal3() {
        Vector normal = new BasicVector(new double[]{-1, 0});
        Vector a = new BasicVector(new double[]{1, 0});
        Vector b = new BasicVector(new double[]{1, 1});
        Vector actual = GeometryUtil.countExternalNormal(normal, a, b);
        Assert.assertEquals(normal.length(), actual.length());
        normal = normal.multiply(-1);
        DoubleAssert.assertEquals(normal.get(0), actual.get(0), 0.1);
        DoubleAssert.assertEquals(normal.get(1), actual.get(1), 0.1);

    }

    @Test
    public void testCountPerimeter() {
        List<Vector> border = Arrays.asList(
                Tool.createVector(0, 0),
                Tool.createVector(1, 0),
                Tool.createVector(0, 1)
        );
        double actual = GeometryUtil.countPerimeter(border);
        double expected = 2.0 + Math.sqrt(2);
        DoubleAssert.assertEquals(expected, actual, 0.001);
    }

    @Test
    public void testCountPerimeter2() {
        int radius = 5;
        Shape ellipse = ShapeFactory.createEllipse(radius, radius, -1, -1, 0, 0.0001);
        double actual = GeometryUtil.countPerimeter(ellipse.getDeckardBorder());
        double expected = 2.0 * Math.PI * radius;
        DoubleAssert.assertEquals(expected, actual, 0.001);
    }

    @Test
    public void testCountPerimeter3() {
        double a = 5;
        double b = 5;
        Shape ellipse = ShapeFactory.createEllipse(a, b, -1, -1, 0, 0.0001);
        double actual = GeometryUtil.countPerimeter(ellipse.getDeckardBorder());
        double v = a - b;
        double expected = 4 * (Math.PI * a * b + v * v) / (a + b);
        DoubleAssert.assertEquals(expected, actual, 0.001);
    }

    @Test
    public void testCountArea() {
        List<Vector> border = Arrays.asList(
                Tool.createVector(0, 0),
                Tool.createVector(1, 0),
                Tool.createVector(0, 1)
        );

        double actual = GeometryUtil.countArea(border);
        double expected = 0.5;
        DoubleAssert.assertEquals(expected, actual, 0.001);
    }

    @Test
    public void testCountArea2() {
        List<Vector> border = Arrays.asList(
                Tool.createVector(1, 1),
                Tool.createVector(3, 1),
                Tool.createVector(3, 3),
                Tool.createVector(1, 3)
        );
        double actual = GeometryUtil.countArea(border);
        double expected = 4;
        DoubleAssert.assertEquals(expected, actual, 0.001);
    }

    @Test
    public void testCountArea3() {
        int radius = 5;
        Shape circle = ShapeFactory.createCircle(radius, -10, -1, 0.0001);
        List<Vector> border = circle.getDeckardBorder();
        double actual = GeometryUtil.countArea(border);
        double expected = Math.PI * radius * radius;
        DoubleAssert.assertEquals(expected, actual, 0.0001);
    }

    @Test
    public void testCountArea4() {
        double a = 5;
        double b = 7;
        Shape ellipse = ShapeFactory.createEllipse(a, b, -10, -1, 0, 0.0001);
        List<Vector> deckardBorder = ellipse.getDeckardBorder();
        double actual = GeometryUtil.countArea(deckardBorder);
        double expected = Math.PI * a * b;
        DoubleAssert.assertEquals(expected, actual, 0.0001);
    }

    @Test
    public void atanTest() {
        DoubleAssert.assertEquals(0.7853, GeometryUtil.angel(1, 1), 0.001);
        DoubleAssert.assertEquals(2.3561, GeometryUtil.angel(-1, 1), 0.001);
        DoubleAssert.assertEquals(5.4977, GeometryUtil.angel(1, -1), 0.001);
        DoubleAssert.assertEquals(3.9269, GeometryUtil.angel(-1, -1), 0.001);
    }

    @Test
    public void testIsInside() {
        Shape circle = ShapeFactory.createCircle(5, 0, 0, 0.1, 0.1);
        assertTrue(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(0, 0)));
        assertTrue(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(4.9, 0)));
        assertTrue(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(-4.9, 0)));
        assertTrue(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(0, 4.9)));
        assertTrue(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(0, -4.9)));
        assertTrue(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(4.999, 0)));
        assertTrue(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(5.0001, 0)));

        assertFalse(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(5.1, 0)));
        assertFalse(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(-5.1, 0)));
        assertFalse(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(0, 5.1)));
        assertFalse(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(0, -5.1)));

        assertFalse(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(0, 4.999)));

        Shape ellipse = ShapeFactory.createEllipse(6, 10, 5, 5, .1, .1);
        assertTrue(GeometryUtil.isInside(ellipse.getDeckardBorder(), Tool.createVector(5, 5)));

        assertFalse(GeometryUtil.isInside(circle.getDeckardBorder(), Tool.createVector(12, 12)));
    }

    @Test
    public void testGetExternalRectangle() throws Exception {
        Shape circle = ShapeFactory.createCircle(5.5, 0, 5.5, .01);
        Vector[] externalRectangle = GeometryUtil.getExternalRectangle(circle.getDeckardBorder());
        Vector v1 = externalRectangle[0];
        Vector v2 = externalRectangle[1];
        DoubleAssert.assertEquals(v1.get(0), -5.5, .1);
        DoubleAssert.assertEquals(v1.get(1), 0, .1);
        DoubleAssert.assertEquals(v2.get(0), 5.5, .1);
        DoubleAssert.assertEquals(v2.get(1), 11, .1);
    }
}
