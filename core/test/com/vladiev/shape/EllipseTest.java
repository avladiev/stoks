package com.vladiev.shape;

import com.vladiev.model.Border;
import jdk.nashorn.internal.ir.annotations.Ignore;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by mazzachuses on 31.01.16.
 */


public class EllipseTest {


    @Test
    @Ignore
    public void test1() {
        Ellipse ellipse = new Ellipse(4, 2, 10, 5, Math.PI*7.0/4, 0.1);
        //Border deckardBorder = ellipse.getDeckardBorder();
        //System.out.println(deckardBorder);
        Border polarBorder = ellipse.getPolarBorder();
        System.out.println(polarBorder);
    }

    @Test
    @Ignore
    public void test2() {
        double r1 = 2;
        double r2 = 3;
        Ellipse ellipse = new Ellipse(r1, r2, 0, 0, Math.PI/3.0, .001);
        Border deckardBorder = ellipse.getDeckardBorder();
        Border polarBorder = ellipse.getPolarBorder();
        System.out.println(polarBorder);
    }
}